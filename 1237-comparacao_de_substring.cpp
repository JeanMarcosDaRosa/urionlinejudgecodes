//Comparação de Substring

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

size_t compara(string in1, string in2){
    size_t s1=in1.size(), s2=in2.size(), cont=0, maior=0, i=0, j=0, aux=0;
    while (i < s1) {
            aux = i;
            while ((j < s2) && (in1[i] != in2[j])) j++;
            while ((j < s2) && (i < s1)) {
                if((in1[i] == in2[j])){
                    j++;i++;
                    if (++cont > maior) maior = cont;
                }else{
                    cont=0;
                    i = aux;
                    j++;
                }
            }
            i = aux + 1;
            cont = j = 0;
        }
        return maior;
}

int main(){
    string in1, in2;
    while(getline(cin, in1)){
        getline(cin, in2);
        size_t a = compara(in1, in2), b = compara(in2, in1);
        printf("%d\n", a>b?a:b);
    }
    return EXIT_SUCCESS;
}
