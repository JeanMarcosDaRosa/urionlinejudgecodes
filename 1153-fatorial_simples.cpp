//1153 - Fatorial Simples
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i,f, fat=1;
    scanf("%d", &f);
    for(i=1;i<=f;i++){
        fat*=i;
    }
    printf("%d\n", fat);
    return 0;
}
