//Loop Musical
#include <stdio.h>
#include <stdlib.h>

int main(){
    int casos, pico, a, ultimo = 0;
    bool sobe = true;
    scanf("%d", &casos);
    while(casos!=0){
        int loop[casos];
        pico = 0;
        for(int i=0;i<casos;i++){
            scanf("%d", &loop[i]);
        }
        for(int i=0;i<casos;i++){
            if(i==0){
                if(loop[casos-1]>=loop[i]){
                    sobe = false;
                }else{
                    sobe = true;
                }
            }else if(loop[i-1]<loop[i]){
                if(!sobe){
                    sobe=true;
                    pico++;
                }
            }else if(loop[i-1]>loop[i]){
                if(sobe){
                    sobe=false;
                    pico++;
                }
                
            }
            if(i==(casos-1)){
                if(loop[i]<loop[0]){
                    if(!sobe){
                        sobe=true;
                        pico++;
                    }
                }else{
                    if(sobe){
                        sobe=false;
                        pico++;
                    }
                }
            }
        }
        printf("%d\n", pico);
        scanf("%d", &casos);
    }
    return 0;
}
