//Estiagem

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <map>
#include <math.h>

using namespace std;

int main(){
    int casos, m, c, caso=0;
    while(scanf("%d", &casos)&&casos!=0){
        map<int, int> registro;
        map<int, int>::iterator it;
        int tot_consumo = 0, tot_moradores=0;
        for(int i=0;i<casos;i++){
            scanf("%d %d", &m, &c);
            registro[c/m]+=m;
            tot_consumo+=c;tot_moradores+=m;
        }
        double media = floor(100.0 * tot_consumo / tot_moradores) / 100;
        if(caso) printf("\n");
        caso++;
        printf("Cidade# %d:\n", caso);
        it=registro.begin();
        printf("%d-%d", it->second, it->first);
        for(++it; it!=registro.end(); ++it){
            printf(" %d-%d", it->second, it->first);
        }
        printf("\nConsumo medio: %.2lf m3.\n", media);
    }
    return EXIT_SUCCESS;
}
