//Sequências de Granizo

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    int n;
    while(scanf("%d", &n)&&n){
        int ant = n, m=n;
        while(ant!=1){
            if(ant%2==0){
                n = ant/2;
            }else{
                n = (ant*3)+1;
            }
            if(ant>m) m=ant;
            ant = n;
        }
        printf("%d\n", m);
    }
    return EXIT_SUCCESS;
}
