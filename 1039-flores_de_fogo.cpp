//Flores de Fogo - GEOMETRIA
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

using namespace std;

int main()
{
    float r1, x1, y1, r2, x2, y2, dco;
    while(scanf("%f %f %f %f %f %f", &r1, &x1, &y1, &r2, &x2, &y2)!=EOF){
        //printf("x1-y1=%f\n", abs(x1-y1));
        //printf("x2-y2=%f\n", abs(x2-y2));
        //printf("r1-r2=%f\n", (r1-r2));
        //printf("Soma: %f\n",(abs(x1-y1)+abs(x2-y2)));
        dco = sqrt(pow((x2-x1),2)+pow((y2-y1),2));
        if(dco<=(r1-r2)){
            printf("RICO\n");
        }else{
            printf("MORTO\n");
        }
    }
    return 0;
}
