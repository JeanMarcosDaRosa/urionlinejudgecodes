//1075 - Resto 2
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    int k, i;
    scanf("%d", &k);
    for(i=2;i<10000;i++){
        if(i%k==2){
            printf("%d\n",i);
        }
    }
    return 0;
}
