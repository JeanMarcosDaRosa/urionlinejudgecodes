//Fibonacci em Vetor
#include <stdio.h>
#include <stdlib.h>


int main()
{
    int c, idx;
    unsigned long long int fibo[61];
    fibo[0]=0;
    fibo[1]=1;
    for(int i=2;i<=61;i++){
        fibo[i]=fibo[i-1]+fibo[i-2];
    }
    scanf("%d", &c);
    for(int j=0;j<c;j++){
        scanf("%d", &idx);
        printf("Fib(%d) = %llu\n", idx, fibo[idx]);
    }
    return 0;
}
