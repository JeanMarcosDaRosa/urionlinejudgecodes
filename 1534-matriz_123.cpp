//1534 - Matriz 123

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

int main(){
    int n;
    while(scanf("%d", &n)!=EOF){
        int z = n-1;
        for(int i=0; i<n; i++){
            for(int j=0; j<n; j++){

                if(j==z){
                    printf("2");
                    z--;
                }else if(i==j){
                    printf("1");
                }else printf("3");
            }
            printf("\n");
        }
    }
    return 0;
}
