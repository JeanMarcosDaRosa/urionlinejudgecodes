//Intervalo 2
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int k, x, i, in=0, out=0;
    scanf("%d", &k);
    for(i=0;i<k;i++){
        scanf("%d", &x);
        if(x>=10&&x<=20){
            in++;
        }else{
            out++;
        }
    }
    printf("%d in\n%d out\n", in, out);
    return 0;
}
