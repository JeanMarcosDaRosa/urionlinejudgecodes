//O Qu�o F�cil �...

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <sstream>

using namespace std;

int main(){
    string line, palavra;
    while(getline(cin, line)){
        stringstream ss(line);
        size_t m=0, div=0;
        while(ss >> palavra){
            size_t c=0, t = palavra.size();
            for(int i=0;i<t;i++){
                if(isalpha(palavra[i])||(palavra[i]=='.'&&i==t-1&&t>1&&c--)){
                    c++;
                }else{
                    c=0;
                    break;
                }
            }
            if(c>0){
                m+=c;
                div++;
            }
        }
        if(m>0) m/=div;
        if(m<=3) cout << "250\n"; else
        if(m==4||m==5) cout << "500\n"; else cout << "1000\n";
    }
    return EXIT_SUCCESS;
}
