#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <sstream>

using namespace std;
typedef long long int lli;

lli concat(lli a, size_t b){return ((a*10)+b);};

int main(){
    string in;
    while(getline(cin, in)){
        size_t len = in.size();
        lli inteiro = 0;
        bool erro = true;
        for(int i=0;i<len;i++){
            if(isdigit(in[i])){
                inteiro=concat(inteiro,in[i]-48);
                erro = false;
            }else{
                if(in[i]=='o'||in[i]=='O'){
                    inteiro=concat(inteiro,0);
                    erro = false;
                }else if(in[i]=='l'){
                    inteiro=concat(inteiro,1);
                    erro = false;
                }else if(in[i]!=','&&in[i]!=' '){
                    erro = true;
                    break;
                }
            }
            if(inteiro>2147483647){
                erro = true;
                break;
            }
        }
        if(erro) printf("error\n");
        else printf("%d\n", inteiro);
    }
    return EXIT_SUCCESS;
}
