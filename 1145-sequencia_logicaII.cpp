//Sequ�ncia L�gica 2

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n, d;
    scanf("%d %d", &d, &n);
    for(int i=1;i<=n;i++){
        if(i%d!=0){
            printf("%d ",i);
        }else{
            printf("%d\n", i);
        }
    }
    return 0;
}
