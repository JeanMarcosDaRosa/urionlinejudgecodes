//Jogo do Maior

#include <stdio.h>

int main(){
    int r;
    while(scanf("%d", &r)&&r!=0){
        int a, b, ar=0, br=0;
        for(int i=0;i<r;i++){
            scanf("%d %d", &a, &b);
            if(a>b){
                ar++;
            }else if(a<b){
                br++;
            }
        }
        printf("%d %d\n", ar, br);
    }
    return 0;
}
