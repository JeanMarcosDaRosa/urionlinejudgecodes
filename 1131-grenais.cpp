//Grenais
#include "stdio.h"
#include "stdlib.h"

int main(){
    int op=1, inter, gremio, ti=0, tg=0, count=0, empates=0;
    while(op!=2){
        count++;
        scanf("%d %d", &inter,&gremio);
        if(inter==gremio){
            empates++;
        }else if(inter>gremio){
            ti++;
        }else if(inter<gremio){
            tg++;
        }
        printf("Novo grenal (1-sim 2-nao)\n");
        scanf("%d",&op);
    }
    printf("%d grenais\n", count);
    printf("Inter:%d\n", ti);
    printf("Gremio:%d\n", tg);
    printf("Empates:%d\n", empates);
    if(ti==tg){
        printf("Nao houve vencedor\n");
    }else if(ti>tg){
        printf("Inter venceu mais\n");
    }else if(ti<tg){
        printf("Gremio venceu mais\n");
    }
    return 0;
}
