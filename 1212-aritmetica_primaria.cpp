//Aritmética Primária

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <string.h>

using namespace std;

void convertInt(char* c, int number){
   sprintf(c, "%d", number);
}

int main(){
    int a, b;
    while(scanf("%d %d", &a, &b)&&!(a==0&&b==0)){
        int cont=0;
        char x[10], y[10];
        convertInt(x, a);
        convertInt(y, b);
        string x1 = x, y1 = y;
        if(strlen(x)<strlen(y)){
            swap(x1, y1);
        }
        int ix=x1.size()-1, iy=y1.size()-1, buff=0;
        for(;ix>=0; ix--, iy--){
            if(((isdigit(x1[ix])?(x1[ix]-48):0)+(isdigit(y1[iy])?(y1[iy]-48):0)+buff)>9){
                buff=1;
                cont++;
            }else{
                buff=0;
            }
        }
        if(cont==0) cout << "No carry operation.\n"; else
        if(cont==1) cout << "1 carry operation.\n"; else
        cout << cont << " carry operations.\n";
    }
    return EXIT_SUCCESS;
}
