//Guarda Costeira

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    double d, vf, vg;
    double tf=0, tg=0;
    while(scanf("%lf %lf %lf", &d, &vf, &vg)!=EOF){
        tf=(12/vf);
        tg=(sqrt(pow(12, 2)+pow(d, 2))/vg);
        if(tf<tg) cout << "N\n";
        else cout << "S\n";
    }
    return EXIT_SUCCESS;
}
