//Elevador

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    int l,c,r1,r2;
    while(scanf("%d %d %d %d", &l, &c, &r1, &r2)&&(l||c)){
        int m = l<c?l:c;
        if(r1*2<=m&&r2*2<=m){
            if(hypot(l-r1-r2, c-r1-r2)>=r1+r2){
                printf("S\n");
            }else printf("N\n");
        }else printf("N\n");
    }
    return EXIT_SUCCESS;
}
