//Ones

#include <stdio.h>

int main(){
    int test;
    while (scanf("%d", &test) != EOF){
        unsigned long long qualquer = 1;
        int contador = 1;
        while (true){
            qualquer = ((qualquer * 10) + 1)%test;
            contador++;
            if (qualquer == 0) break;
        }
        printf("%d\n", contador);
    }
    return 0;
}
