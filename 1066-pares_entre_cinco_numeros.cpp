//Pares entre cinco numeros
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

int main()
{
    int i, pares=0, impares=0, positivo=0, negativo=0, v;
    for(i=0;i<5;i++){
        scanf("%d", &v);
        if(v%2==0){
            pares++;
        }else impares++;
        if(v>0){
            positivo++;
        }else if(v<0){
            negativo++;
        }
    }
    cout << pares << " valor(es) par(es)\n";
    cout << impares << " valor(es) impar(es)\n";
    cout << positivo << " valor(es) positivo(s)\n";
    cout << negativo << " valor(es) negativo(s)\n";
    return 0;
}
