#include <iostream>
#include <iomanip>
#define PI 3.14159
 
using namespace std;
 
int main(){
    double area;
    cin >> area;
    double A = PI*(area*area);
    cout << "A="<< fixed << setprecision(4)  << A << endl;
    return 0;
}