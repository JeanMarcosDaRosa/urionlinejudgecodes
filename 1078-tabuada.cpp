//1078 - Tabuada
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    int k, i;
    scanf("%d", &k);
    for(i=1;i<=10;i++){
        printf("%d x %d = %d\n",i,k,i*k);
    }
    return 0;
}
