//Pares entre cinco numeros
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

int main()
{
    int i, c = 0, v;
    for(i=0;i<5;i++){
        scanf("%d", &v);
        if(v%2==0){
            c++;
        }
    }
    cout << c << " valores pares\n";
    return 0;
}
