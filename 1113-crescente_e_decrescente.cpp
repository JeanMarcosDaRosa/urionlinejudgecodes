//Crescente e Decrescente
#include <stdio.h>
#include <stdlib.h>

int main()
{
   int x, y;
   while(scanf("%d %d", &x, &y)&&(x!=y)){
       if(x>y) printf("Decrescente\n");
       else printf("Crescente\n");
   }
   return 0;
}
