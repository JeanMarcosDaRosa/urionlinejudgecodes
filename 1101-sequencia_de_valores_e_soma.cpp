//Sequencia de Valores e Soma
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
   int x, y, s;
   while(scanf("%d %d", &x, &y)&&(x>0&&y>0)){
       s=0;
       if(y<x) swap(x, y);
       for(int j=x;j<=y;j++){
           printf("%d ", j);
           s+=j;
       }
       printf("Sum=%d\n", s);
   }
   return 0;
}
