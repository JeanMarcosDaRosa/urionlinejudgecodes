//Bingo!

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <vector>
#include <algorithm>

using namespace std;

int compare (const void * a, const void * b){
  return ( *(int*)a - *(int*)b );
}

bool possivel(int n, vector<int> vet){
    for(vector<int>::iterator itPoint=vet.begin();itPoint!=vet.end();++itPoint){
        for(vector<int>::iterator itSec=itPoint;itSec!=vet.end();++itSec){
            if((*itSec-*itPoint)==n){
                return true;
            }
        }
    }
    return false;
}

int main(){
    int t, f, valor;
    while(scanf("%d %d", &t, &f)&&!(t==0&&f==0)){
        vector<int> vet;
        for(int i=0;i<f;i++){
            scanf("%d", &valor);
            vet.push_back(valor);
        }
        qsort(&vet[0], vet.size(), sizeof(int), compare);
        bool elim = false;
        bool possible = true;
        if(vet[0]==0){
            elim = true;
        }
        for(int i=0;i<=t;i++){
            if(!(elim && binary_search(vet.begin(), vet.end(), i))){
                if(!possivel(i, vet)){
                    possible = false;
                    break;
                }
            }
        }
        if(possible){
            cout << "Y\n";
        }else cout << "N\n";
    }
    return EXIT_SUCCESS;
}
