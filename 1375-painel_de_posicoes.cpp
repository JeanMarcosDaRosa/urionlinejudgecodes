//Painel de Posi��es

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <cstring>

using namespace std;

int main(){
    int casos, carro, posi;
    while(scanf("%d", &casos)&&casos!=0){
        int vet[casos];
        memset(vet, 0, sizeof(vet));
        bool valido = true;
        for(int i=0;i<casos;i++){
            scanf("%d %d", &carro, &posi);
            if((valido)&&((i+posi<casos)&&(i+posi>=0))&&(vet[i+posi]==0)){
                vet[i+posi] = carro;
            }else{
                valido = false;
            }
        }
        if(valido){
            printf("%d", vet[0]);
            for(int i=1;i<casos;i++){
                printf(" %d", vet[i]);
            }
            printf("\n");
        }else{
            printf("-1\n");
        }
    }
    return EXIT_SUCCESS;
}
