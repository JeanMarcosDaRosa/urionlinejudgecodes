//Dividindo X por Y
#include <stdio.h>
#include <stdlib.h>

int main()
{
   float x,y,c;
   scanf("%f", &c);
   for(int i=0;i<c;i++){
       scanf("%f %f", &x, &y);
       if(y==0){
           printf("divisao impossivel\n");
       }else{
           printf("%.1f\n", (x/y));
       }
   }
   return 0;
}
