//Sequências Crescentes

#include <stdio.h>

int main()
{
    int x = 0;
    while(scanf("%d", &x)&&x!=0){
        for(int i=1;i<=x;i++){
            printf("%d", i);
            if(i!=x)printf(" ");
            else printf("\n");
        }
    }
    return 0;
}
