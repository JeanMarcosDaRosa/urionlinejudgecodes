#include <stdio.h>
#include <queue>

#define TAM 1000

using namespace std;

int s[TAM];
int q[TAM];

int main() {

	int count;
	while (scanf("%d", &count) != EOF) {

        priority_queue<int> pq;
		int isSt, isQu, isPq;
		int sTop, qHead, qTail;
		qHead = qTail = sTop = 0;
		isSt = isQu = isPq = 1;

		for (int a, b, i = 0; i < count; i++) {
			scanf("%d %d", &a, &b);

			if ( a == 1 ) {
				s[sTop++] = b;
				q[qTail++] = b;
				pq.push(b);
			}
			else {
                // if do top tá errado
                if (s[--sTop] != b) {
                    isSt = 0;
                }
                if (q[qHead++] != b){
                    isQu = 0;
                }
                if (pq.top() != b) {
                    isPq = 0;

                } else if (!pq.empty()){
                    pq.pop();
                }
			}
		}

		if ( (isSt + isQu + isPq) > 1 ) {
			printf("not sure\n", isSt, isQu, isPq);
		}
		else if ( isSt == 1 ) {
			printf("stack\n");
		}
		else if ( isQu == 1 ) {
			printf("queue\n");
		}
		else if ( isPq == 1 ) {
			printf("priority queue\n");
		}
		else {
			printf("impossible\n");
		}
	} // fim do while
} // fim do main
