//Árvore Binária de Busca

#include <cstdio>
#include <cstdlib>
#include <iostream>

using namespace std;

typedef struct nodo {
    nodo *esq;
    nodo *dir;
    int valor;
} nodo;

//Pre: raiz, esq, dir
//In:  esq, raiz, dir
//Pos: esq, dir, raiz

void insere(nodo* &raiz, int valor){
    if (raiz==NULL) {
        nodo *a = new nodo;
        a->esq = NULL;
        a->dir = NULL;
        a->valor = valor;
        raiz = a;
    } else {
        if(valor < raiz->valor) {
            insere(raiz->esq, valor);
        } else {
            insere(raiz->dir, valor);
        }
    }
}

void prefixo(nodo *raiz) {
    if(raiz!=NULL){
        printf(" %d", raiz->valor);
        if (raiz->esq != NULL) {
            prefixo(raiz->esq);
        } if (raiz->dir != NULL) {
            prefixo(raiz->dir);
        }
    }

}

void infixo(nodo *raiz) {
    if(raiz!=NULL){
        if (raiz->esq != NULL) {
            infixo(raiz->esq);
        }
        printf(" %d", raiz->valor);
        if (raiz->dir != NULL) {
            infixo(raiz->dir);
        }
    }
}

void posfixo(nodo *raiz) {
    if(raiz!=NULL){
        if (raiz->esq != NULL) {
            posfixo(raiz->esq);
        }
        if (raiz->dir != NULL) {
            posfixo(raiz->dir);
        }
        printf(" %d", raiz->valor);
    }
}

int main(){
    int n, v;
    scanf("%d", &n);
    for (int loop = 0; loop < n; loop++) {
        int tam;
        nodo *raiz = NULL;
        scanf("%d", &tam);
        for (int i = 0; i < tam; i++) {
            scanf("%d", &v);
            insere(raiz, v);
        }
        printf("Case %d:\n", loop+1);
        printf("Pre.:");
        prefixo(raiz);
        printf("\n");
        printf("In..:");
        infixo(raiz);
        printf("\n");
        printf("Post:");
        posfixo(raiz);
        printf("\n\n");
    }
}
