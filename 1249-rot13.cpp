//Rot13

#include <iostream>
#include <stdio.h>

using namespace std;

int main(){
    string in;
    while(getline(cin, in)){
        for(int i=0;i<in.size();i++){
            int x = in[i];
            if(x>=65&&x<=90){
                x=(x+13)%91;
                if(x<65) x+=65;
                printf("%c", x);
            }else if(x>=97&&x<=122){
                x=(x+13)%123;
                if(x<97) x+=97;
                printf("%c", x);
            }else{
                printf("%c", in[i]);
            }
        }
        printf("\n");
    }
    return 0;
}
