//Idades

#include <stdio.h>

int main()
{
    int x, c=0;
    float s=0.00;
    while(scanf("%d", &x)&&x>=0){
        s+=x;
        c++;
    }
    printf("%.2f\n", s/c);
    return 0;
}
