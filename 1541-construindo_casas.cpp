//1541 - Construindo Casas

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cmath>

using namespace std;

int main(){
    int a, b, p;
    while(scanf("%d", &a)&&a!=0){
        scanf("%d %d", &b, &p);
        double m = (double)(((a*b)*100)/p);
        int l = sqrt(m);
        printf("%d\n", l);
    }
    return 0;
}
