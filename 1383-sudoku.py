# Sudoku

def equals(x, y):
    if len(x) != len(y): return False
    x, y = sorted(set(x)), sorted(set(y))
    for i in range(0, len(x)):
        if x[i] != y[i]:
            return False
    return True

def verifica(matriz):
    base = list(range(1, 10))
    ok = True
    for i in range(0, 9):
        if not equals(matriz[i], base):
            ok = False
            break
        tmp = []
        for x in range(0, 9):
            tmp.append(matriz[x][i])
        if not equals(tmp, base):
            ok = False
            break
    if ok:
        tmp = []
        for i in range(0, 3):
            for x in range(0, 3):
                tmp.append(matriz[i][x])
        if not equals(tmp, base):
            ok = False
    if ok:
        tmp = []
        for i in range(0, 3):
            for x in range(3, 6):
                tmp.append(matriz[i][x])
        if not equals(tmp, base):
            ok = False
    if ok:
        tmp = []
        for i in range(0, 3):
            for x in range(6, 9):
                tmp.append(matriz[i][x])
        if not equals(tmp, base):
            ok = False
    if ok:
        tmp = []
        for i in range(3, 6):
            for x in range(0, 3):
                tmp.append(matriz[i][x])
        if not equals(tmp, base):
            ok = False
    if ok:
        tmp = []
        for i in range(3, 6):
            for x in range(3, 6):
                tmp.append(matriz[i][x])
        if not equals(tmp, base):
            ok = False
    if ok:
        tmp = []
        for i in range(3, 6):
            for x in range(6, 9):
                tmp.append(matriz[i][x])
        if not equals(tmp, base):
            ok = False
    if ok:
        tmp = []
        for i in range(6, 9):
            for x in range(0, 3):
                tmp.append(matriz[i][x])
        if not equals(tmp, base):
            ok = False
    if ok:
        tmp = []
        for i in range(6, 9):
            for x in range(3, 6):
                tmp.append(matriz[i][x])
        if not equals(tmp, base):
            ok = False
    if ok:
        tmp = []
        for i in range(6, 9):
            for x in range(6, 9):
                tmp.append(matriz[i][x])
        if not equals(tmp, base):
            ok = False

    print('SIM\n') if ok else print('NAO\n')

n = int(input())
for t in range(0, n):
    matriz = []
    for j in range(0, 9):
        matriz.append([int(x) for x in input().split(' ')])
    print("Instancia", int(t)+1)
    verifica(matriz)

