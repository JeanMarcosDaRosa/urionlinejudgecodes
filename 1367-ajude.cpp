//Ajude!

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <map>

using namespace std;

typedef struct{
    int somaTime;
    int penalidade;
    bool correct;
} pontuacao;

int main(){
    int c, time, penality;
    char prob;
    string resp;
    bool correct = false;
    while(scanf("%d ", &c)&&c!=0){
        map<char, pontuacao> placar;
        for(int i=0;i<c;i++){
            cin >> prob >> time >> resp;
            penality=0;
            if(resp=="incorrect"){
                correct = false;
                penality=20;
            } else correct = true;
            if(placar.find(prob)!=placar.end()){
                placar[prob].somaTime=time;
                placar[prob].penalidade+=penality;
                if(correct){
                    placar[prob].correct = true;
                }else placar[prob].correct = false;
            }else{
                pontuacao p;
                p.somaTime = time;
                p.penalidade = penality;
                if(correct){
                    p.correct = true;
                }else p.correct = false;
                placar[prob]=p;
            }
        }
        int count_s = 0, soma_s = 0;
        for(map<char, pontuacao>::iterator it=placar.begin();it!=placar.end();++it){
            if(it->second.correct){
                count_s++;
                soma_s+=it->second.somaTime+it->second.penalidade;
            }
        }
        cout << count_s << " " << soma_s << "\n";
    }
    return EXIT_SUCCESS;
}
