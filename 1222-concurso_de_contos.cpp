//Concurso de Contos

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;


int main(){
    int qtdWord, nroLinhas, qtdChar, acum, totLinhas, totPages, size_word;
    string word;
    while(scanf("%d %d %d ", &qtdWord, &nroLinhas, &qtdChar)!=EOF){
        acum=0;
        totPages=totLinhas=1;
        for(int i=0;i<qtdWord;i++){
            cin >> word;
            size_word = word.size();
            if(acum>0)acum++;
            if(acum+size_word>qtdChar){
                acum=0;
                totLinhas++;
            }
            if(totLinhas>nroLinhas){
                totLinhas=1;
                totPages++;
            }
            acum+=size_word;
        }
        cout << totPages << "\n";
    }
    return EXIT_SUCCESS;
}
