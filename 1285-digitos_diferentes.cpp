//D�gitos diferentes

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <string.h>
#include <sstream>

using namespace std;

void convertInt(char* c, int number){
   sprintf(c, "%d", number);
}

int main(){
    int n, m;
    while(scanf("%d %d", &n, &m)!=EOF){
        int t=0;
        for(int i=n;i<=m;i++){
            char val[4];
            convertInt(val, i);
            int l = strlen(val);
            bool possivel = true;
            for(int x=0;x<l;x++){
                for(int j=x+1;j<l;j++){
                    if(val[x]==val[j]){
                        possivel = false;
                        break;
                    }
                }
            }
            if(possivel) t++;
        }
        printf("%d\n", t);
    }
    return EXIT_SUCCESS;
}
