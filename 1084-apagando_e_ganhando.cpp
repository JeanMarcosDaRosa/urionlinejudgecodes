//Apagando e Ganhando

#include <cstdio>
#include <deque>
#include <iostream>

using namespace std;

int main(){
    deque<char> digitos;
    int n, d, tam, i;
    string num;
    while (scanf("%d %d ", &n, &d) && n){
        tam = n - d;
        getline(cin, num);
        for (i = 0; i < n; i++){
            while (!digitos.empty() && d > 0 && num[i] > digitos.back()){
                digitos.pop_back();
                d--;
            }
            if ((int)digitos.size() < tam){
                digitos.push_back(num[i]);
            }
        }
        for (deque<char>::iterator it = digitos.begin(); it != digitos.end(); it++){
            putchar(*it);
        }
        putchar('\n');
        digitos.clear();
    }
    return 0;
}
