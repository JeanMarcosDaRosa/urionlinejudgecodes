//Figurinhas
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>

using namespace std;

int mdc(int a,int b){
	if(b == 0) return a;  
	else return mdc(b,a%b); 
} 

int main(){
    register int casos, tam, x, y;
    scanf("%d", &casos);
    for(int i=0;i<casos;i++){
        scanf("%d %d", &x, &y);
        printf("%d\n", mdc(x, y));
    }
    return 0;
}