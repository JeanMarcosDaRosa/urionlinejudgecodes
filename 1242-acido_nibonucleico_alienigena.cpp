//Ácido Ribonucleico Alienígena

#include <iostream>
#include <cstdlib>
#include <stack>

using namespace std;

int main(){
    string in;
    while(getline(cin, in)){
        int tam = in.size();
        int cont = 0;
        stack<char> pilha;

        pilha.push(in[0]);

        for (int i=1; i<tam; i++) {
            if(!pilha.empty() && (
                (in[i] == 'B' && pilha.top() == 'S') ||
                (in[i] == 'S' && pilha.top() == 'B') ||
                (in[i] == 'F' && pilha.top() == 'C') ||
                (in[i] == 'C' && pilha.top() == 'F'))) {
                pilha.pop();
                cont++;
            }else{
                pilha.push(in[i]);
            }
        }
        cout << cont << endl;
    }
    return EXIT_SUCCESS;
}
