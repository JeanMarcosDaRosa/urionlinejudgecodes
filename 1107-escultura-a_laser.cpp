//Escultura � Laser

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <vector>
#include <algorithm>

using namespace std;

int main(){
    int h, w, v;
    while(scanf("%d %d", &h, &w)!=EOF&&(h!=0||w!=0)){
        int x, a=h, t = 0;
        for(int i=0;i<w;i++){
            scanf("%d", &v);
            if(v<a){
                t+=a-v;
            }
            a=v;
        }
        printf("%d\n", t);
    }
    return EXIT_SUCCESS;
}
