//Justificador

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <iomanip>

using namespace std;

int main(){
    int c, sizein, aux;
    string in;
    bool first = true;
    while(scanf("%d ", &c)&&(c!=0)){
        string vec[c];
        sizein = 0;
        for(int i=0;i<c;i++){
            cin >> in;
            vec[i] = in;
            aux = in.size();
            if(aux>sizein)
                sizein = aux;
        }
        if(!first) cout << "\n";
        first = false;
        for(int i=0;i<c;i++){
            cout << right << setw(sizein) << vec[i] << "\n";
        }
    }
    return EXIT_SUCCESS;
}
