//Numerando Estradas

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <math.h>

using namespace std;

int main(){
    int c=1, t;
    double r, n;
    while(scanf("%lf %lf", &r, &n)!=EOF&&(r!=0||n!=0)){
        t=ceil((r-n)/n);
        //t=(r/n);
        if(t<27) printf("Case %d: %d\n", c++, t);
        else printf("Case %d: impossible\n", c++);
    }
    return EXIT_SUCCESS;
}
