#include <iostream>
#include <cstdlib>
#include <stdio.h>

using namespace std;

int mdc(int a, int b){
    while(b>0){
        a = a%b;
        a^= b;
        b^= a;
        a^= b;
    }
    return a;
}

int abs(int a){
    return a>=0?a:(a*-1);
}

int main(){
    int casos, n1, n2, d1, d2, sn, sd;
    char op;
    scanf("%d", &casos);
    for(int i=0;i<casos;i++){
        scanf("%d / %d %c %d / %d", &n1, &d1, &op, &n2, &d2);
        switch(op){
            case '+':
                sn = (n1*d2)+(n2*d1);
                sd = d1*d2;
                break;
            case '-':
                sn = (n1*d2)-(n2*d1);
                sd = d1*d2;
                break;
            case '*':
                sn = n1*n2;
                sd = d1*d2;
                break;
            case '/':
                sn = n1*d2;//n1/d1
                sd = n2*d1;//n2/d2
                break;
        }
        int x = mdc(abs(sn),abs(sd));
        printf("%d/%d = %d/%d\n", sn, sd, sn/x, sd/x);
    }
    return 0;
}
