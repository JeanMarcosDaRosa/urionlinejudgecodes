//Bakugan

#include <stdio.h>

int main(){
    int c, m;
    while(scanf("%d", &c)&&c!=0){
        int j1=0, j2=0;
        int v1[c], v2[c];
        bool extra = false;
        for(int i=0;i<c;i++){
            scanf("%d", &v1[i]);
        }
        for(int i=0;i<c;i++){
            scanf("%d", &v2[i]);
        }
        for(int i=0;i<c;i++){
            j1+=v1[i];
            j2+=v2[i];
            if(i>=2){
                if(v1[i]==v1[i-1]&&v1[i]==v1[i-2]){
                    if (!(v2[i]==v2[i-1]&&v2[i]==v2[i-2])&&!extra){
                        j1+=30;
                    }
                    extra = true;
                }
                if(v2[i]==v2[i-1]&&v2[i]==v2[i-2]){
                    if(!(v1[i]==v1[i-1]&&v1[i]==v1[i-2])&&!extra){
                        j2+=30;
                    }
                    extra = true;
                }
            }
        }
        if(j1==j2) printf("T\n");else
        if(j1<j2) printf("L\n");else
        if(j1>j2) printf("M\n");
    }
    return 0;
}
