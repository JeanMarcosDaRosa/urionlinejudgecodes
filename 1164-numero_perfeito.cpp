#include <stdio.h>
#include <stdlib.h>
 
void num_perfeito(int num);
 
int main(){
    int casos, i, num;
    scanf("%d", &casos);
    for(i=0; i<casos;i++){
        scanf("%d", &num);
        num_perfeito(num);
    }
    return 0;
}
 
void num_perfeito(int num){
    int i, soma_div = 0;
    for(i = 1; i<num;i++){
        if(num%i==0){
            soma_div += i;
        }
    }
    if(soma_div==num){
        printf("%d eh perfeito\n", num);
    }else{
        printf("%d nao eh perfeito\n", num);
    }
