//Linha na Matriz

#include <stdio.h>

int main()
{
    int x = 5;
    float v=0;
    char op='S';
    float m[12][12];
    scanf("%d %c%*c", &x, &op);
    for(int i=0;i<12;i++){
        for(int j=0;j<12;j++){
            scanf("%f", &m[i][j]);
        }
    }
    for(int i=0;i<12;i++){
        v+=m[x][i];
    }
    if(op=='S'){
        printf("%.1f\n", v);
    }else if(op=='M'){
        printf("%.1f\n", v/12);
    }
    return 0;
}
