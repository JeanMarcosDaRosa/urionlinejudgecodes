//Leitura M�ltipla

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    string in;
    int proc, size_in, oper, buff=0;
    while(getline(cin, in)){
       scanf("%d", &proc);
       getchar();
       buff=oper=0;
       size_in = in.size();
       for(int i=0;i<size_in;i++){
            if(in[i]=='W'){
                buff = 0;
                oper++;
            }else if(in[i]=='R'){
               if(buff==0) oper++;
               buff++;
               if(buff>=proc){
                    buff = 0;
               }
            }
       }
       cout << oper << "\n";
    }
    return EXIT_SUCCESS;
}
