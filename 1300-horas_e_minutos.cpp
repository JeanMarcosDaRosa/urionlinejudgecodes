//Horas e Minutos

#include <iostream>
#include <stdio.h>

using namespace std;

int main(){
    int valor;
    while(scanf("%d", &valor)!=EOF){
        if(valor%6>0){
            cout << "N\n";
        }else{
            cout << "Y\n";
        }
    }
    return 0;
}