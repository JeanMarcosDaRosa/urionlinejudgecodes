# 1021 - Notas e Moedas

__author__ = 'jean'

val = float(input())

print('NOTAS:')
cont = 0
while val-100.00 >= 0:
    val -= 100.00
    cont += 1
print("%d nota(s) de R$ 100.00" % cont)
cont = 0
while val-50.00 >= 0:
    val -= 50.00
    cont += 1
print("%d nota(s) de R$ 50.00" % cont)
cont = 0
while val-20.00 >= 0:
    val -= 20.00
    cont += 1
print("%d nota(s) de R$ 20.00" % cont)
cont = 0
while val-10.00 >= 0:
    val -= 10.00
    cont += 1
print("%d nota(s) de R$ 10.00" % cont)
cont = 0
while val-5.00 >= 0:
    val -= 5.00
    cont += 1
print("%d nota(s) de R$ 5.00" % cont)
cont = 0
while val-2.00 >= 0:
    val -= 2.00
    cont += 1
print("%d nota(s) de R$ 2.00" % cont)

print('MOEDAS:')
cont = 0
while val-1.00 >= 0:
    val -= 1.00
    cont += 1
print("%d moeda(s) de R$ 1.00" % cont)
cont = 0
while val-0.50 >= 0:
    val -= 0.50
    cont += 1
print("%d moeda(s) de R$ 0.50" % cont)
cont = 0
while val-0.25 >= 0:
    val -= 0.25
    cont += 1
print("%d moeda(s) de R$ 0.25" % cont)
cont = 0
while val-0.10 >= 0:
    val -= 0.10
    cont += 1
print("%d moeda(s) de R$ 0.10" % cont)
cont = 0
while val-0.05 >= 0:
    val -= 0.05
    cont += 1
print("%d moeda(s) de R$ 0.05" % cont)
cont = 0
while val-0.01 >= 0:
    val -= 0.01
    cont += 1
print("%d moeda(s) de R$ 0.01" % cont)
