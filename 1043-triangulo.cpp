//Tri�ngulo
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    float a, b, c;
    scanf("%f %f %f", &a, &b, &c);
    if((a==b||a==c||b==c)){
        printf("Perimetro = %.1f\n", a+b+c);
    }else{
        printf("Area = %.1f\n", (c*(a+b))/2.0);
    }
    return 0;
}