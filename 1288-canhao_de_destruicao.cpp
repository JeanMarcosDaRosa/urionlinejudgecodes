//Canhão de Destruição

#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

// A utility function that returns maximum of two integers
int max(int a, int b) { return (a > b)? a : b; }
 
// Returns the maximum value that can be put in a knapsack of capacity W
int knapSack(int W, int wt[], int val[], int n){
   int i, w;
   int K[n+1][W+1];
   // Build table K[][] in bottom up manner
   for (i = 0; i <= n; i++){
       for (w = 0; w <= W; w++){
           if (i==0 || w==0)
               K[i][w] = 0;
           else if (wt[i-1] <= w)
                 K[i][w] = max(val[i-1] + K[i-1][w-wt[i-1]],  K[i-1][w]);
           else
                 K[i][w] = K[i-1][w];
       }
   }
   return K[n][W];
}

int main(){
    int casos, n, k, r;
    scanf("%d", &casos);
    while(casos--){
        scanf("%d", &n);
        int wt[n];
        int val[n];
        for(int i=0;i<n;i++){
            scanf("%d", &val[i]);
            scanf("%d", &wt[i]);
        }
        scanf("%d", &k);
        scanf("%d", &r);
        (knapSack(k, wt, val, n)>=r)?printf("Missao completada com sucesso\n"): printf("Falha na missao\n");
    }
    return 0;
}