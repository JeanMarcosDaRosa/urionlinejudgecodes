//Esquerda, Volver!
#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

char vira(char posicao, char cmd){
    char r=posicao;
    if(cmd=='D'){
        switch(posicao){
            case 'N':
                r='L';
                break;
            case 'L':
                r='S';
                break;
            case 'S':
                r='O';
                break;
            case 'O':
                r='N';
                break;
        }
    }else if(cmd=='E'){
        switch(posicao){
            case 'N':
                r='O';
                break;
            case 'O':
                r='S';
                break;
            case 'S':
                r='L';
                break;
            case 'L':
                r='N';
                break;
        }
    }
    return r;
}

int main(){
    int c;
    char cmd;
    while(scanf("%d ", &c)&&c!=0){
        char ori = 'N';
        for(int i=0;i<c;i++){
            scanf("%c", &cmd);
            ori = vira(ori, cmd);
        }
        printf("%c\n", ori);
    }
    return 0;
}
