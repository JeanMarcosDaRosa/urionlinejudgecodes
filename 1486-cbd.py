__author__ = 'jean.marcos'
# 1486 - Circuito Bioquimico Digital

while(True):
    inp = input().split(' ')
    p,n,c = int(inp[0]), int(inp[1]), int(inp[2])
    if p==0 and n==0 and c==0: break
    lista = [0 for x in range(0, p)]
    cont = 0
    for i in range(0, n):
        val = input().split(' ')
        for j in range(0, len(val)):
            if val[j] == '0':
                if lista[j] >= c:
                    cont+=1
                lista[j] = 0
            else:
                lista[j] += 1
    for x in range(0, p):
        if lista[x]>=c:
            cont+=1
    print(cont)