#include <stdio.h>
#include <string.h>
 
void lerString(char *str);
void cifraCesar (char *str, int pulo);
 
int main(){
    char texto[50];
    int ct, p, i;
    scanf("%d", &ct);
    for(i = 0; i < ct; i++){
        lerString(texto);
        scanf("%d", &p);
        cifraCesar(texto, p);
    }
    return 0;
}
 
void lerString(char *str){
    int i = 0;
    char a;
    getchar(); //Aqui é uma pausa para digitar a String
    while(1){
        scanf("%c", &a);
        if(a != '\n'){
            str[i] = a;
            i++;
        }else{
            break;
        }
    }
    str[i] = '\0';
}
 
void cifraCesar (char *str, int pulo){
    int i = 0;
    char vet[strlen(str)];
    while(str[i] != '\0'){
        if((str[i] - pulo) >= 65){
            vet[i] = str[i]-pulo;
        }else{
            int temp;
            temp = str[i]-pulo;
            temp = 64 - temp;
            temp = 90 - temp;
            vet[i] = temp;
        }
        i++;
    }
    vet[i] = '\0';
    printf("%s\n", vet);
}