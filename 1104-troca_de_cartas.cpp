//Troca de Cartas

#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <algorithm>
#include <vector>
#include <set>

using namespace std;

int main(){
    int a, b, val;
    while(scanf("%d %d", &a, &b)&&!(a==0&&b==0)){
        set<int> resultA, resultB, c1, c2;
        for(int i=0;i<a;i++){
            scanf("%d", &val);
            c1.insert(val);
        }
        for(int i=0;i<b;i++){
            scanf("%d", &val);
            c2.insert(val);
        }
        vector<int> diffA(a), diffB(b);
        vector<int>::iterator it;

        it=set_difference(c1.begin(), c1.end(), c2.begin(), c2.end(), diffA.begin());
        diffA.resize(it-diffA.begin());

        it=set_difference(c2.begin(), c2.end(), c1.begin(), c1.end(), diffB.begin());
        diffB.resize(it-diffB.begin());

        int sA = diffA.size();
        int sB = diffB.size();
        cout << (sA<sB?sA:sB) << "\n";
    }
    return EXIT_SUCCESS;
}
