//Convers�o simples de Base

#include <cstdlib>
#include <stdio.h>

int main(){
    int a;
    char x[12];
    while(scanf("%s", &x)!=EOF&&(x[0]!='-')){
        sscanf(x, "%i", &a);
        if(x[1]=='x'){
            //HEX -> DEC
            printf("%d\n", a);
        }else{
            //DEC -> HEX
            printf("0x%X\n", a);
        }
    }
    return EXIT_SUCCESS;
}
