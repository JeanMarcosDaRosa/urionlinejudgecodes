//Pares e Ímpares

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <vector>
#include <algorithm>

using namespace std;


int comparePares (const void * a, const void * b){
  return ( *(int*)a - *(int*)b );
}

int compareImpares (const void * a, const void * b){
  return ( *(int*)b - *(int*)a );
}

int main(){
    int casos, val;
    vector<int> pares, impares;
    scanf("%d", &casos);
    for(int i=0;i<casos;i++){
        scanf("%d", &val);
        if(val%2==0){
            pares.push_back(val);
        }else{
            impares.push_back(val);
        }
    }

    qsort(&pares[0], pares.size(), sizeof(int), comparePares);
    qsort(&impares[0], impares.size(), sizeof(int), compareImpares);
    for(vector<int>::iterator it=pares.begin();it!=pares.end();++it){
        cout << *it << "\n";
    }
    for(vector<int>::iterator it=impares.begin();it!=impares.end();++it){
        cout << *it << "\n";
    }
    return EXIT_SUCCESS;
}
