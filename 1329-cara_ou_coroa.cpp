//Cara ou coroa
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main()
{
   int c;
   while(scanf("%d", &c)&&c!=0){
       int j=0, m=0, k;
       for(int i=0;i<c;i++){
           scanf("%d", &k);
           if(k==0){
               m++;
           }else if(k==1){
               j++;
           }
       }
       printf("Mary won %d times and John won %d times\n", m, j);
   }
   return 0;
}
