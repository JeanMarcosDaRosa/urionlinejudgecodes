#include <stdio.h>
#include <stdlib.h>
 
int main(){
    int valor;
    int c = 0;
    int vv[] = {100, 50, 20, 10, 5, 2 ,1};
    scanf("%d", &valor);
    printf("%d\n", valor);
    while(valor!=0){
        int i;
        for(i=0;i<7;i++){
            if(valor/vv[i] >= 1){
                printf("%d nota(s) de R$ %d,00\n", (valor/vv[i]), vv[i]);
                valor = (valor%vv[i]);
            }else{
                printf("0 nota(s) de R$ %d,00\n", vv[i]);
            }
        }
    }
    return 0;
}