//Preenchimento de Vetor II
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

using namespace std;

int main()
{
    int v[1000];
    int a, c=0;
    scanf("%d", &a);
    for(int i=0;i<1000;i++){
        v[i]=c;
        c++;
        if(c>=a)c=0;
    }
    for(int i=0;i<1000;i++){
        printf("N[%d] = %d\n",i, v[i]);
    }
    return 0;
}
