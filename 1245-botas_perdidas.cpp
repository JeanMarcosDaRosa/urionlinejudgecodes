#include <stdio.h>
#include <stdlib.h>

using namespace std;

struct bota{
    int tamanho;
    char lado;
    bool temPar;
};

bool ehPar(bota a, bota b){
    //printf("Comparar %d-%c:%d-%c\n", a.tamanho, a.lado, b.tamanho, b.lado);
    return (a.tamanho == b.tamanho && a.lado != b.lado && (!a.temPar&&!b.temPar));
}

int main(){
    int qtdBotas;
    while(scanf("%d", &qtdBotas)!=EOF){
		bota lista[qtdBotas];
		for(int i=0; i<qtdBotas;i++){
			bota b;
			scanf("%d %c", &b.tamanho, &b.lado);
			b.temPar = false;
			lista[i] = b;
		}
		int pares = 0;
		for(int i=0;i<qtdBotas-1;i++){
			for(int j=i+1;j<qtdBotas;j++){
				if((!lista[i].temPar)&&(!lista[j].temPar)){
					if(ehPar(lista[i], lista[j])){
						lista[i].temPar = lista[j].temPar = true;
						pares++;
						break;
					}
				}
			}
		}
		printf("%d\n",pares);
	}
    return 0;
}
