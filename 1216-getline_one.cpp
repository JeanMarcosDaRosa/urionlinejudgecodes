//Getline One

#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

int main(){
    string aux;
    double a, b, c=0, m=0;
    while(getline(cin, aux)){
        scanf("%lf", &a);
        printf("%.1lf\n", a);
        m+=a;
        c++;
    }
    printf("%.1lf\n", m/c);
    return 0;
}
