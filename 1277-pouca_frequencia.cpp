//Pouca Frequência

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    int c, size_v;
    string freq;
    scanf("%d ", &c);
    for(int i=0;i<c;i++){
        scanf("%d ", &size_v);
        string veta[size_v];
        bool first = true;
        for(int k=0;k<size_v;k++){
            cin >> veta[k];
        }
        for(int k=0;k<size_v;k++){
            cin >> freq;
            int s = freq.size();
            float qtda = 0, tot = s;
            for(int j=0;j<s;j++){
                if(freq[j]=='P'){
                    qtda++;
                }else if(freq[j]=='M'){
                    tot--;
                }
            }
            if(((qtda/tot)*100)<75.0){
                if(first){
                    cout << veta[k];
                    first = false;
                } else{
                    cout << " " << veta[k];
                }
            }
        }
        cout << "\n";
    }
    return EXIT_SUCCESS;
}
