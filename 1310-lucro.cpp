#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <cstring>

using namespace std;

int max(int a, int b){return a > b ? a : b;}

int main(){
    int dias, custo, max_so_far, max_ending_here;
    while(scanf("%d", &dias)!=EOF){
        int vet[dias];
        memset(vet, 0, dias);
        scanf("%d", &custo);
        for(int i=0;i<dias;i++){
            scanf("%d", &vet[i]);
            vet[i]-=custo;
        }
        max_ending_here = 0, max_so_far = 0;
        for (int i = 0; i < dias; i++)
        {
            max_ending_here = max(0, max_ending_here + vet[i]);
            max_so_far = max(max_so_far, max_ending_here);
        }
        printf("%d\n", max_so_far);
    }
    return EXIT_SUCCESS;
}
