//Loteria de Fim de Semana

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <cstring>

using namespace std;

int main(){
    int n,c,k,x;
    while(scanf("%d %d %d", &n, &c, &k)&&(n!=0||c!=0||k!=0)){
        vector<int> sorteio(k, 0);
        bool primeiro = true;
        for(int i=0;i<n;i++){
            for(int j=0;j<c;j++){
                scanf("%d", &x);
                sorteio[x-1]++;
            }
        }
        int menor = sorteio[0];
        for(int i=1;i<k;i++) if(sorteio[i]<menor) menor = sorteio[i];
        for(int i=0;i<k;i++){
            if(sorteio[i]==menor){
                if(primeiro){
                    printf("%d", i+1);
                    primeiro=false;
                }else{
                    printf(" %d", i+1);
                }
            }
        }
        putchar('\n');
    }
    return EXIT_SUCCESS;
}
