//Soma de �mpares Consecutivos II
#include <stdio.h>
#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
   int casos;
   scanf("%d", &casos);
   for(int i;i<casos;i++){
       int x, y, c=0;
       scanf("%d %d", &x, &y);
       if(y<x) swap(x, y);
       for(int j=x+1;j<y;j++){
           if(j%2!=0){
               c+=j;
           }
       }
       printf("%d\n", c);
   }
   return 0;
}
