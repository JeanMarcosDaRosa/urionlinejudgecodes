//Frequência de Letras

#include <iostream>
#include <stdio.h>
#include <map>
#include <string>
#include <string.h>
#include <algorithm>

using namespace std;

struct LessBySecond
{
    template <typename Lhs, typename Rhs>
    bool operator()(const Lhs& lhs, const Rhs& rhs) const
    {
        return lhs.second < rhs.second;
    }
};


int main(){
    int casos;
    map<char, int> m;
    string in;
    scanf("%d ", &casos);
    for(int i=0;i<casos;i++){
        getline(cin, in);
        transform(in.begin(), in.end(),in.begin(), ::tolower);
        for(string::iterator it=in.begin();it!=in.end();++it){
            if(isalpha(*it)){
                if(m.find(*it)==m.end()){
                    m[*it]=1;
                }else{
                    m[*it]++;
                }
            }
        }
        int maxv = max_element(m.begin(), m.end(), LessBySecond())->second;
        for (map<char, int>::iterator it = m.begin(); it!=m.end(); it++){
            if(it->second==maxv){
                cout << it->first;
            }
        }
        cout << "\n";
        m.clear();
    }
}
