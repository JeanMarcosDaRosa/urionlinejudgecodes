//Sub-prime

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <vector>

using namespace std;

int main(){
    int qtdB, qtdDb, valor, dev, crd;
    while(scanf("%d %d", &qtdB, &qtdDb)&&!(qtdB==0&&qtdDb==0)){
        vector<int> db;
        for(int i=0;i<qtdB;i++){
            scanf("%d", &valor);
            db.push_back(valor);
        }
        for(int i=0;i<qtdDb;i++){
            scanf("%d %d %d", &dev, &crd, &valor);
            db[crd-1]+=valor;
            db[dev-1]-=valor;
        }
        bool devedores = false;
        for(int i=0;i<qtdB;i++){
            if(db[i]<0){
                devedores = true;
                break;
            }
        }
        if(devedores){
            cout << "N\n";
        }else{
            cout << "S\n";
        }
    }
    return EXIT_SUCCESS;
}
