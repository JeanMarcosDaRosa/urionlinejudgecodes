//Rob� Colecionador
#include<stdio.h>
#include<ctype.h>

typedef struct{
    char ori;
    int i, j;
} robo;

void mudaOri(robo &r, char cmd){
    //printf("De %c para ", r.ori);
    if(cmd=='D'){
		switch(r.ori){
			case 'N':
				r.ori = 'L';
				break;
			case 'S':
				r.ori = 'O';
				break;
			case 'O':
				r.ori = 'N';
				break;
			case 'L':
				r.ori = 'S';
				break;
		}
	}else if(cmd=='E'){
		switch(r.ori){
			case 'N':
				r.ori = 'O';
				break;
			case 'S':
				r.ori = 'L';
				break;
			case 'O':
				r.ori = 'S';
				break;
			case 'L':
				r.ori = 'N';
				break;
		}
	}
	//printf("%c\n", r.ori);
}

int main() {
	int n, m, c;
	while(scanf("%d %d %d", &n, &m, &c)&&(n!=0&&m!=0&&c!=0)){
		char matriz[n][m];
		robo r;
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				//printf("\nmatriz[%d][%d]: ", i, j);
				while(scanf("%c", &matriz[i][j])&&matriz[i][j]=='\n');
				�//printf("%c", matriz[i][j]);
				if(isalpha(matriz[i][j])){
					r.ori = matriz[i][j];
					r.i = i;
					r.j = j;
				}
			}
			//printf("\n");
		}
		int qF = 0;
		for(int i=0;i<c;i++){
			char cmd;
			while(scanf("%c", &cmd)&&cmd=='\n');
			if(cmd=='F'){
				if(r.ori=='N'&&(matriz[r.i-1][r.j]!='#')&&(r.i-1>=0)){
					matriz[r.i][r.j] = '.';
					r.i--;
				}
				if(r.ori=='S'&&(matriz[r.i+1][r.j]!='#')&&(r.i+1<n)) {
					matriz[r.i][r.j] = '.';
					r.i++;
				}
				if(r.ori=='O'&&(matriz[r.i][r.j-1]!='#')&&(r.j-1>=0)){
					matriz[r.i][r.j] = '.';
					r.j--;
				}
				if(r.ori=='L'&&(matriz[r.i][r.j+1]!='#')&&(r.j+1<m)){
					matriz[r.i][r.j] = '.';
					r.j++;
				}
				//printf("Vai para o %c i:%d, j:%d\n", r.ori, r.i, r.j);
			}else{
				mudaOri(r, cmd);
			}
			if(matriz[r.i][r.j]=='*'){
				qF++;
				matriz[r.i][r.j] = '.';
			}
		}
		printf("%d\n", qF);
	}
}