//Prefixa, infixa e posfixa

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <stack>
using namespace std;

void proc(string pre, string in, int &pivo_pre, int pos_ini, int pos_fim){
    if(pos_ini>pos_fim) return;
    char pivo = pre[pivo_pre];
    int i = pos_ini;
    while(in[i]!=pivo&&i<pos_fim)i++;
    if(in[i]==pivo)pivo_pre++;
    proc(pre, in, pivo_pre, pos_ini, i-1);
    proc(pre, in, pivo_pre, i+1, pos_fim);
    printf("%c", in[i]);
}

int main(){
    int casos;
    scanf("%d", &casos);
    for(int k=0;k<casos;k++){
        string pre;
        string in;
        int s, i=0, j=0;
        scanf("%d ", &s);
        cin >> pre >> in;
        int x = 0;
        proc(pre, in, x, 0, s-1);
        putchar('\n');
    }
    return EXIT_SUCCESS;
}
