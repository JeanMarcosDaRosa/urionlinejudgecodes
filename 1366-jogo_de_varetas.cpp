#include <stdio.h>
#include <stdlib.h>

using namespace std;

struct varetas{
    int tam;
    int qtd;
};

int main(){
    int casos;
    scanf("%d",&casos);
    while(casos!=0){
        varetas lista[casos];
        int retangulos = 0;
        for(int i=0;i<casos;i++){
            varetas v;
            scanf("%d %d", &v.tam, &v.qtd);
            v.qtd = v.qtd-(v.qtd % 2);
            retangulos += (v.qtd/4);
            v.qtd = v.qtd % 4;
            lista[i] = v;
        }
        int restantes = 0;
        for(int i=0;i<casos;i++){
            restantes+=lista[i].qtd;
        }
        retangulos+= restantes / 4;
        printf("%d\n", retangulos);
        scanf("%d",&casos);
    }
    return 0;
}
