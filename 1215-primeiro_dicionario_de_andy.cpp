//Primeiro Dicionário de Andy

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <set>
#include <algorithm>
#include <string>
#include <limits>

using namespace std;

bool invalidChar (char c){
    return !isalpha (c);
}

int main(){
    string in;
    char c = '\0';
    set<string> dic;
    while(scanf("%c", &c)!=EOF){
        if(isalpha(c)){
            in+=tolower(c);
        }else{
            if(!in.empty()){
                dic.insert(in);
                in.clear();
            }
        }
    }
    if(!in.empty()){
        dic.insert(in);
        in.clear();
    }
    for(set<string>::iterator it=dic.begin();it!=dic.end();++it){
        cout << *it << "\n";
    }



    return EXIT_SUCCESS;
}
