//Formata��o Monet�ria

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    string in, cents, out;
    while(cin >> in){
        cin >> cents;
        int size_in = in.size();
        int cont = 3;
        out = "";
        for(int i=size_in-1;i>=0;i--, cont--){
            if(cont==0){
                out.insert(out.begin(), ',');
                out.insert(out.begin(), in[i]);
                cont = 3;
            }else{
                out.insert(out.begin(), in[i]);
            }
        }
        if(cents.size()<2){
            out += ".0"+cents;
        }else{
            out += "."+cents;
        }
        cout << "$" << out << "\n";
    }
    return EXIT_SUCCESS;
}
