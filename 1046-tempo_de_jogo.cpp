//Tempo de Jogo
#include <stdio.h>
#include <stdlib.h>

int main(){
    int hi, hf;
    scanf("%d %d", &hi, &hf);
    if(hi>hf){
        printf("O JOGO DUROU %d HORA(S)\n", (24-hi)+hf);
    }else{
        printf("O JOGO DUROU %d HORA(S)\n", hf-hi);
    }
    return 0;
}