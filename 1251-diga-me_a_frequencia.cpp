//Diga-me a Frequência

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <map>
#include <vector>
#include <algorithm>

using namespace std;

typedef struct {
    int ascii;
    int cont;
} letra;

bool comparar(const letra &a, const letra &b){
    if(a.cont == b.cont){
        return b.ascii<a.ascii;
    }
    return (a.cont < b.cont);
}

int main(){
    string in;
    bool frescuraDoUri = false;
    while(getline(cin, in)){
        int in_size = in.size();
        map<char, int> mapa;
        vector<letra> order;
        for(int i=0;i<in_size;i++){
            if(mapa.find(in[i])!=mapa.end()){
                mapa[in[i]]++;
            }else{
                mapa[in[i]]=1;
            }
        }
        for(map<char, int>::iterator it=mapa.begin(); it!=mapa.end(); ++it){
            letra a;
            a.ascii = (int)it->first;
            a.cont  = it->second;
            order.push_back(a);
        }
        sort(order.begin(), order.end(), comparar);
        if(frescuraDoUri) cout << "\n";
        frescuraDoUri = true;
        for(vector<letra>::iterator it=order.begin(); it!=order.end(); ++it){
            cout << it->ascii << " " << it->cont << "\n";
        }
    }
    return EXIT_SUCCESS;
}
