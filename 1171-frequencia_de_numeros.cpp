//Frequencia de Numeros

#include <stdio.h>
#include <iostream>
#include <map>

using namespace std;

int main(){
    int c, x;
    map<int, int> num;
    scanf("%d", &c);
    for(int i=0;i<c;i++){
        scanf("%d", &x);
        if(num.find(x)==num.end()){
            num[x]=1;
        }else{
            num[x]++;
        }
    }
    for(map<int, int>::iterator it=num.begin(); it!=num.end(); ++it){
        cout << it->first << " aparece " << it->second << " vez(es)\n";
    }
    return 0;
}
