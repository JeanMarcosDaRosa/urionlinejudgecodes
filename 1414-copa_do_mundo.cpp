//Copa do Mundo

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    int t, n, p;
    string nome;
    while((cin >> t >> n)&&(t!=0)){
        int pontos = n*3;
        for(int i=0;i<t;i++){
            cin >> nome >> p;
            pontos-=p;
        }
        printf("%d\n", pontos);
    }
    return EXIT_SUCCESS;
}
