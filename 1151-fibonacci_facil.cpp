//Fibonacci Facil
#include <stdio.h>
#include <stdlib.h>


int main()
{
    int c;
    scanf("%d", &c);
    int fibo[c];
    fibo[0]=0;
    fibo[1]=1;
    for(int i=0;i<c;i++){
        if(i>1){
            fibo[i]=fibo[i-1]+fibo[i-2];
        }        
        printf("%d", fibo[i]);
        if(i!=c-1){
            printf(" ");
        }
    }
    printf("\n");
    return 0;
}
