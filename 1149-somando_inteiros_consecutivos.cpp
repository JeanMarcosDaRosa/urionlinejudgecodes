//Somando Inteiros consecutivos

#include <stdio.h>

int main()
{
    int a = 0, n = 0, s=0, c=1;
    scanf("%d", &a);
    while(scanf("%d", &n)&&n<=0);
    s=a;
    while(c<n){
        s+=a+c;
        c++;
    }
    printf("%d\n", s);
    return 0;
}
