//Quadrado e ao Cubo
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    int valor;
    scanf("%d", &valor);
    for(int i=1; i<=valor;i++){
        printf("%.0f %.0f %.0f\n", pow(i, 1), pow(i, 2), pow(i, 3));
    }
    return 0;
}