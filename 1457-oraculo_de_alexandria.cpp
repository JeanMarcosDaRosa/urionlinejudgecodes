//Or�culo de Alexandria

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>

using namespace std;

unsigned long long oraculo(int n, int dif){
    unsigned long long fat=1;
    for(int i=n;i>=1;i-=dif){
        fat*=i;
    }
    return fat;
}

int main(){
    int f;
    cin >> f;
    string a;
    for(int i=0;i<f;i++){
        int n;
        scanf("%d ", &n);
        cin >> a;
        printf("%llu\n", oraculo(n, a.size()));
    }
    return 0;
}
