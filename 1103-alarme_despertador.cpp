//Alarme Despertador
#include <stdio.h>
#include <stdlib.h>

int main(){
    int hi, mi, hf, mf, h, m;
    while(scanf("%d %d %d %d", &hi, &mi, &hf, &mf)&&(hi!=0||mi!=0||hf!=0||mf!=0)){
        h=hf-hi;
        m=mf-mi;
        if(h<0) h+=24;
        if(m<0){
            m+=60;
            h--;
            if(h<0) h+=24;
        }
        printf("%d\n", (h*60)+m);
    }
    return 0;
}
