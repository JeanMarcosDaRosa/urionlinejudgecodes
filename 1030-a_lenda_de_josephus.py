def last_to_die(n, k):
    ret = 1
    for n in range(1, n+1):
        ret = (ret+k-1) % n+1
    return ret

for i in range(0, int(input())):
    dados = input().split(' ')
    print('Case {}: {}'.format(i+1, last_to_die(int(dados[0]), int(dados[1]))))