//�rea
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    float a, b, c;
    scanf("%f %f %f", &a, &b, &c);
    
    
    printf("TRIANGULO: %.3f\n", (a*c)/2.0);
    printf("CIRCULO: %.3f\n", 3.14159*pow(c,2));
    printf("TRAPEZIO: %.3f\n", (c*(a+b))/2.0);
    printf("QUADRADO: %.3f\n", pow(b,2));
    printf("RETANGULO: %.3f\n", a*b);
    return 0;
}