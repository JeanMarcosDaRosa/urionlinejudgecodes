//Soma de impares consecutivos
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, a, b ,x, y, t=0;
    scanf("%d %d", &x, &y);
    if(x<y){
        a=x;
        b=y;
    }else{
        a=y;
        b=x;
    }
    for(i=a+1;i<x;i++){
        if(i%2!=0) t+=i;
    }
    printf("%d\n", t);
    return 0;
}
