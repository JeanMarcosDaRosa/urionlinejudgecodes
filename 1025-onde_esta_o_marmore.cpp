//Onde est� o M�rmore?

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <vector>
#include <algorithm>

using namespace std;

int compare (const void * a, const void * b){
  return ( *(int*)a - *(int*)b );
}

int main(){
    int n, q, in, c=1, pos;
    int * pItem;
    vector<int>::iterator it;
    while(scanf("%d %d", &n, &q)&&(n!=0||q!=0)){
        vector<int> vet;
        for(int i=0;i<n;i++){
            scanf("%d", &in);
            vet.push_back(in);
        }
        if(vet.size()>0) qsort(&vet[0], vet.size(), sizeof(int), compare);
        cout << "CASE# "<<c<<":\n";
        for(int i=0;i<q;i++){
            scanf("%d", &in);
            pItem = (int*) bsearch (&in, &vet[0], vet.size(), sizeof(int), compare);
            if(pItem!=NULL){
                while(pItem!=NULL){
                    pos = (pItem-&vet[0]);
                    pItem = (int*) bsearch (&in, &vet[0], pos, sizeof(int), compare);
                }
                cout << in << " found at " << pos+1 << "\n";
            }else{
                cout << in << " not found\n";
            }
        }
        c++;
    }
    return EXIT_SUCCESS;
}
