//Pontes de São Petersburgo

#include <stdio.h>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <vector>

using namespace std;

typedef vector<int> vi;
typedef vector<vi> vii;
typedef vector<bool> vb;

/*
        v[1..n] contém os graus de cada vértice
        ok[0..K] guarda se a soma i pode ser atingida, começa com 0
        ok[0] = 1
        para cada vértice v[i] :
        para j de K até v[i] :
        decresce para evitar repetição
        se (ok[j − v[i]] == 1):
        ok[j] = 1
        se a soma j − v[i] é possível, então j também é.
        Assim basta verificar depois se ok[K] é 1.
*/

int main() {
    int i, r, k, r1, r2;
    while (scanf("%d %d", &r, &k) != EOF) {
        int vet[r];
        bool ok[k+1];
        memset(vet, 0, sizeof (vet));
        memset(ok, 0, sizeof (ok));
        for (i = 0; i < k; i++) {
            scanf("%d %d", &r1, &r2);
            vet[r1 - 1] += 1;
            vet[r2 - 1] += 1;
        }
        ok[0] = 1;
        for (int i=0;i<r;i++)
            for (int j = k; j >= vet[i]; j--)
                if (ok[j - vet[i]] == 1) ok[j] = 1;
        printf(ok[k]?"S\n":"N\n");
    }
    return 0;
}
