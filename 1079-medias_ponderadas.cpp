//M�dias Ponderadas
#include <iostream>
#include <stdio.h>

using namespace std;

int main()
{
   int casos;
   scanf("%d", &casos);
   for(int i=0;i<casos;i++){
        float a,b,c;
        scanf("%f %f %f", &a,&b,&c);
        printf("%.1f\n", (a*0.20)+(b*0.30)+(c*0.50));
   }
   return 0;
}
