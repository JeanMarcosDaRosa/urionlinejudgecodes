//Soma de Pares Consecutivos
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

using namespace std;

int main()
{
    int a, c=0;
    while(scanf("%d", &a)&&a!=0){
        c=0;
        if(a%2!=0) a++;
        for(int i=0;i<10;i+=2){
            c+=a+i;
        }
        printf("%d\n",c);
    }
    return 0;
}
