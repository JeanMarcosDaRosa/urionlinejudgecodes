# Bazinga!

x = int(input())

for i in range(0, int(x)):
	y = input().split(' ')
	she, raj = y[0], y[1]
	if she == raj:
		print("Caso #%d: De novo!" % (i+1))
	else:
		if (she == 'tesoura' and ( 'lagarto' == raj or raj == 'papel' )) \
			or (she == 'papel' and ( 'pedra' == raj or raj == 'Spock' )) \
			or (she == 'pedra' and ( 'lagarto' == raj or raj == 'tesoura' )) \
			or (she == 'lagarto' and ( 'Spock' == raj or raj == 'papel' )) \
			or (she == 'Spock' and ( 'tesoura' == raj or raj == 'pedra' )):
			print("Caso #%d: Bazinga!" % (i+1))
		else:
			print("Caso #%d: Raj trapaceou!" % (i+1))
'''
3
papel pedra
lagarto tesoura
Spock Spock

Caso #1: Bazinga!
Caso #2: Raj trapaceou!
Caso #3: De novo!
'''