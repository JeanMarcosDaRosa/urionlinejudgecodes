//Flores Florescem da Fran�a

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <string>
#include <sstream>

using namespace std;

int main(){
    string in, aux;
    while(true){
        getline(cin, in);
        if(in=="*") break;
        stringstream ss(in);
        char first;
        ss >> aux;
        first = toupper(aux[0]);
        bool tau = true;
        while(ss >> aux){
            if(toupper(aux[0])!=first){
                tau = false;
                break;
            }
        }
        if(tau){
            cout << "Y\n";
        }else{
            cout << "N\n";
        }
    }
    return EXIT_SUCCESS;
}
