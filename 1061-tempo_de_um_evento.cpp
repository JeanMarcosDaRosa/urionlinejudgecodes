//Tempo de um Evento
#include <stdio.h>
#include <stdlib.h>

int main(){
   int di, df, hi, hf, mi, mf, si, sf, d, h, m, s;
   scanf("%*s %d %*c %d : %d : %d", &di, &hi, &mi, &si);
   scanf("%*s %d %*c %d : %d : %d", &df, &hf, &mf, &sf);
   s = sf-si;
   m = mf-mi;
   h = hf-hi;
   d = df-di;
   if(s<0){
       s+=60;
       m--;
   }
   if(m<0){
       m+=60;
       h--;
   }
   if(h<0){
       h+=24;
       d--;
   }
   if(d<0){
       h+=30;
   }

   printf("%d dia(s)\n%d hora(s)\n%d minuto(s)\n%d segundo(s)\n",d, h, m, s);
   return 0;
}
