//Numero Primo
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

using namespace std;

bool num_primo(int ult_primo){
    int a, flag=1;
    float n;
    if(ult_primo==2) return true;
    while(1){
        if(ult_primo>7)
        while(ult_primo%2==0||ult_primo%3==0||ult_primo%5==0||ult_primo%7==0) return false;
        n=sqrt(ult_primo);
        for(a=2;a<=n;a++){
            if(ult_primo%a==0){
                return false;
            }
        }
        if(flag) return true;
    }
}

int main()
{
    int casos = 0;
    scanf("%d", &casos);
    for(int i=0;i<casos;i++){
        int num;
        scanf("%d", &num);
        if(num_primo(num)){
            printf("%d eh primo\n", num);
        }else{
            printf("%d nao eh primo\n", num);
        }
    }
    return 0;
}
