//Gerando Permutações Ordenadas Rapidamente

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <string.h>
#include <algorithm>

using namespace std;

int main(){
    int casos, len;
    char palavra[11];
    scanf("%d ", &casos);
    for(int i=0;i<casos;i++){
        len = strlen(gets(palavra));
        sort(palavra,palavra+len);
        do{
            puts(palavra);
        }while(next_permutation(palavra, palavra+len));
        putchar('\n');
    }
    return EXIT_SUCCESS;
}
