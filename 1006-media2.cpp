//Media 2
#include <stdio.h>
#include <stdlib.h>

int main(){
    double a, b, c, media;
    scanf("%lf %lf %lf", &a, &b, &c);
    media = (a*0.2)+(b*0.3)+(c*0.5);
    printf("MEDIA = %.5lf\n",media);
    return 0;
}