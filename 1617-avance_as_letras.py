#Avance as Letras

x = int(input())

for i in range(0, x):
	a, b = tuple(input().split(' '))
	cont = 0
	for i in range(0, len(a)):
		rA = ord(a[i])
		rB = ord(b[i])
		if rA > rB:
			cont += (122 - rA) + (rB - 97) + 1
		else:
			cont += rB - rA
	print(cont)