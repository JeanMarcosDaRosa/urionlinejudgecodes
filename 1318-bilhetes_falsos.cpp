//Bilhetes Falsos

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <set>

using namespace std;

int main(){
    int a, b, x;
    while(scanf("%d %d", &a, &b)&&!(a==0&&b==0)){
        set<int> lista, res;
        for(int i=0;i<b;i++){
            scanf("%d", &x);
            if(lista.find(x)!=lista.end()){
                res.insert(x);
            }else{
                lista.insert(x);
            }
        }
        cout << res.size() << "\n";
    }
    return EXIT_SUCCESS;
}
