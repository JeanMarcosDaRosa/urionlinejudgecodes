//Ordenação por Tamanho

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <sstream>

using namespace std;

bool compare_string_size (string i,string j){
  return (j.size()<i.size());
}

int main(){
    int casos;
    string linha, in;
    scanf("%d ", &casos);
    for(int i=0;i<casos;i++){
        getline(cin, linha);
        vector<string> vetor;
        stringstream ss(linha);
        while(ss >> in){
            vetor.push_back(in);
        }

        stable_sort (vetor.begin(), vetor.end(), compare_string_size);

        int vet_size=vetor.size();
        for(int i=0;i<vet_size;i++){
            cout << vetor[i];
            if (i<vet_size-1) cout << " ";
        }
        cout << "\n";
    }
    return EXIT_SUCCESS;
}
