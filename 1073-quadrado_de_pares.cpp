//Quadrado de Pares
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    int k, i;
    scanf("%d", &k);
    for(i=1;i<=k;i++){
        if(i%2==0){
            printf("%d^2 = %.0f\n", i, i, pow(i, 2));
        }
    }
    return 0;
}
