//Abaixo da Diagonal Principal

#include <stdio.h>

int main()
{
    int x = 5;
    float v=0, c=0;
    char op='S';
    float m[12][12];
    scanf("%c%*c", &op);
    for(int i=0;i<12;i++){
        for(int j=0;j<12;j++){
            scanf("%f", &m[i][j]);
        }
    }
    for(int i=0;i<12;i++){
        for(int j=0;j<i;j++){
            c++;
            v+=m[i][j];
        }
    }
    if(op=='S'){
        printf("%.1f\n", v);
    }else if(op=='M'){
        printf("%.1f\n", v/c);
    }
    return 0;
}
