//Lista telef�nica econ�mica

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <vector>
#include <algorithm>

using namespace std;

int main(){
    int c;
    while(scanf("%d", &c)!=EOF){
        vector<string> lista;
        vector<string>::iterator it;
        string in, a, b;
        int eco=0,t;
        for(int i=0;i<c;i++){
            cin >> in;
            lista.push_back(in);
        }
        sort(lista.begin(), lista.end());
        for(int i=0;i<lista.size()-1;i++){
            a = lista[i];
            b = lista[i+1];
            t=0;
            while(a[t]==b[t]){
                eco++;
                t++;
            }
        }

        printf("%d\n", eco);
    }
    return 0;
}
