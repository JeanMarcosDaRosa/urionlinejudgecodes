#include <stdio.h>
 
void preenche_vet(int *vet, int t);
 
int main()
{
    int N;
    do
    {
        int i, j, pulo=1;
        scanf("%d", &N);
 
        //Se N for igual a zero então encerra a execução do programa
        if(N == 0)
        {
            break;
        }
        while(1)
        {
            int desligados = 0;
            int vet[N];
            int idx = 0,aux = 0;
            int debug = 0;
            preenche_vet(vet, N);
            while(vet[idx]!=13)
            {
                vet[idx] = 0;
                desligados++;
                aux=0;
                while(aux<pulo)
                {
                    idx++;
                    if(idx>=N) idx=0;
                    if(vet[idx]!=0) aux+=1;
                }
            }
            if((desligados==N)||(desligados==(N-1)))
            {
                printf("%d\n", pulo);
                break;
            }
            pulo++;
        }
    }
    while(N!=0);
}
 
void preenche_vet(int *vet, int t)
{
    int i;
    for(i=0; i<t; i++) vet[i]=i+1;
}