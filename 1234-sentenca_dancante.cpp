//Senten�a Dan�ante

#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <ctype.h>

using namespace std;

int main(){
    string in;
    while(getline(cin, in)){
        bool up = false;
        transform(in.begin(), in.end(), in.begin(), ::tolower);
        for(int i=0;i<in.size();i++){
            if(isalpha(in[i])){
                if(up){
                    printf("%c", in[i]);
                    up=false;
                }else{
                    printf("%c", toupper(in[i]));
                    up=true;
                }
            }else{
                printf("%c", in[i]);
            }
        }
        printf("\n");
    }
    return 0;
}
