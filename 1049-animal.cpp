//Animal
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <string.h>

using namespace std;

int main()
{
    string c1, c2, c3;
    cin >> c1;
    cin >> c2;
    cin >> c3;
    if(c1=="vertebrado"){
        if(c2=="ave"){
            if(c3=="carnivoro"){
                printf("aguia\n");
            }else if(c3=="onivoro"){
                printf("pomba\n");
            }
        }else if(c2=="mamifero"){
            if(c3=="onivoro"){
                printf("homem\n");
            }else if(c3=="herbivoro"){
                printf("vaca\n");
            }
        }
    }else if(c1=="invertebrado"){
        if(c2=="inseto"){
            if(c3=="hematofago"){
                printf("pulga\n");
            }else if(c3=="herbivoro"){
                printf("lagarta\n");
            }
        }else if(c2=="anelideo"){
            if(c3=="hematofago"){
                printf("sanguessuga\n");
            }else if(c3=="onivoro"){
                printf("minhoca\n");
            }
        }
    }
    return 0;
}
