# Triangulos
__author__ = 'jean'

while True:
    try:
        s = int(input())
        lista = input().split(' ')
        pontos = set([0])
        soma = 0
        quantidade = 0
        for i in lista:
            soma += int(i)
            pontos.add(soma)

        if soma % 3 == 0:
            passo = int(soma/3)
            for p in pontos:
                if p < passo:
                    if p + passo in pontos and p + (passo * 2) in pontos:
                        quantidade += 1
        print(quantidade)
    except Exception as a:
        break
