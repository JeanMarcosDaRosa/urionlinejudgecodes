//Fibonacci, quantas chamadas?
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int fib(int n, bool getC){
    static int count = 0;
    if(getC){
        int a = count;
        count = 0;
        return a;
    }
    count++;
    if(n==0) return 0;
    if(n==1) return 1;
    return (fib(n-1, false) + fib(n-2, false));
}

int main()
{
    int c;
    scanf("%d", &c);
    for(int i=0;i<c;i++){
        int n, f, q;
        scanf("%d", &n);
        f = fib(n, false);
        q = fib(0, true);
        if(n==0||n==1) q=2;
        printf("fib(%d) = %d calls = %d\n", n, --q, f);
    }
    return 0;
}
