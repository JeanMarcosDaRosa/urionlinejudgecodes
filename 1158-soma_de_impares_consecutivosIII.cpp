//Soma de Impares Consecutivos III

#include <stdio.h>

int main()
{
   int casos;
   scanf("%d", &casos);
   for(int i=0;i<casos;i++){
       int x, y, c=0;
       scanf("%d %d", &x, &y);
       while(y>0){
           if(x%2!=0){
               c+=x++;
               y--;
           }
           x++;
       }
       printf("%d\n", c);
   }
   return 0;
}

