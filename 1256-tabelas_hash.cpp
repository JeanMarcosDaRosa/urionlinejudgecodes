//Tabelas Hash

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <vector>

using namespace std;

int main(){
    int cases, h, t, v;
    vector<int> vec;
    scanf("%d", &cases);
    for(int i=0;i<cases;i++){
        scanf("%d %d", &h, &t);
        vector<int> tab[h];
        for(int j=0;j<t;j++){
            scanf("%d", &v);
            tab[(v % h)].push_back(v);
        }
        for(int j=0;j<h;j++){
            if(tab[j].size()>0){
                cout << j ;
                for(vector<int>::iterator it=tab[j].begin();it!=tab[j].end();++it){
                    cout << " -> " << *it;
                }
                cout << " -> \\\n";
            }else{
                cout << j << " -> \\\n";
            }
        }
        if(i<cases-1) cout << "\n";
    }
    return EXIT_SUCCESS;
}
