//Troca em vetor I
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

using namespace std;

int main()
{
    int v[20];
    for(int i=0;i<20;i++){
        scanf("%d", &v[i]);
    }
    for(int i=0, j=19;i<10;i++, j--){
        swap(v[i], v[j]);
    }
    for(int i=0;i<20;i++){
        printf("N[%d] = %d\n",i, v[i]);
    }
    return 0;
}
