//Trigo no Tabuleiro
#include <iostream>
#include <stdint.h>
using namespace std;

int main()
{
   int k;
   uint64_t graos;
   cin>>k;
   for(int i=0;i<k;i++){
       int x;
       cin >> x;
       graos = 1;
       for(int j=0;j<x;j++){
            graos*=2;   
       }
       graos--;
       cout << (graos/12)/1000 << " kg\n";
   }
   return 0;
}
