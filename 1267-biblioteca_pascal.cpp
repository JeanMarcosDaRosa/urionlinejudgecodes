//Biblioteca Pascal

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    int a, j;
    while(scanf("%d %d", &a, &j)&&(a!=0||j!=0)){
        int m[j][a];
        for(int l=0; l<j; l++){
            for(int c=0;c<a;c++){
                scanf("%d", &m[l][c]);
            }
        }
        bool todos;
        for(int c=0; c<a; c++){
            todos = true;
            for(int l=0;l<j;l++){
                if(m[l][c]==0){
                    todos = false;
                    break;
                }
            }
            if(todos){
                break;
            }
        }
        if(todos){
            cout << "yes\n";
        }else{
            cout << "no\n";
        }
    }
    return EXIT_SUCCESS;
}
