//Grid de Largada


#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

using namespace std;

int main(){
    int c;
    while(scanf("%d", &c)!=EOF){
        int inicio[c];
        int fim[c];
        int trocas=0;
        for(int i=0;i<c;i++)scanf("%d", &inicio[i]);
        for(int i=0;i<c;i++)scanf("%d", &fim[i]);
        int idv = 0;
        int x = 0;
        while(idv<c){
            int i=0;
            for(;i<c;i++)
                if(inicio[i]==fim[idv]) break;
            while(inicio[idv]!=fim[idv]){
                swap(inicio[i-1], inicio[i]);
                i--;
                trocas++;
            }
            idv++;
        }
        printf("%d\n", trocas);
    }
    return 0;
}
