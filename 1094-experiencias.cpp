//ExperiÍncias
#include <stdlib.h>
#include <stdio.h>

int main(){
    float s=0,r=0,c=0,t=0,qtd=0;
    int casos=0;
    char tipo;
    scanf("%d", &casos);
    for(int i=0;i<casos;i++){
        scanf("%f %c", &qtd, &tipo);
        if(tipo=='C')c+=qtd;
        if(tipo=='R')r+=qtd;
        if(tipo=='S')s+=qtd;
    }
    t = (c+s+r);
    printf("Total: %.0f cobaias\n", t);
    printf("Total de coelhos: %.0f\n",c);
    printf("Total de ratos: %.0f\n",r);
    printf("Total de sapos: %.0f\n",s);
    printf("Percentual de coelhos: %.2f \%\n", (c/t)*100);
    printf("Percentual de ratos: %.2f \%\n", (r/t)*100);
    printf("Percentual de sapos: %.2f \%\n", (s/t)*100);
    return 0;
}