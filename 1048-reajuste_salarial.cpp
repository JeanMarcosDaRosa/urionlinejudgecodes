//reajuste salarial
#include <stdio.h>
#include <stdlib.h>

int main(){
    float s, r;
    scanf("%f", &s);
    if(s>0&&s<=400) r = 15;
    if(s>400&&s<=800) r = 12;
    if(s>800&&s<=1200) r = 10;
    if(s>1200&&s<=2000) r = 7;
    if(s>2000) r = 4;
    printf("Novo salario: %.2f\n", s+(s*(r/100)));
    printf("Reajuste ganho: %.2f\n", s*(r/100));
    printf("Em percentual: %.0f %\n", r);
    return 0;
}