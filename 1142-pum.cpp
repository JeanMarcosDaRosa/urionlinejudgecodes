//PUM
#include <stdio.h>
#include <stdlib.h>

int main()
{
   int n, l=0, c=1;
   scanf("%d", &n);
   while(l<n){
       if(c%4!=0){
           printf("%d ", c);
       }else{
           printf("PUM\n");
           l++;
       }
       c++;
   }
   return 0;
}
