//Problema F�cil de Rujia Liu?

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <map>
#include <vector>

using namespace std;

int main(){
    int n, m, x, o;
    while(scanf("%d %d", &n, &m)!=EOF){
        map<int, vector<int> > v;
        for(int i=0;i<n;i++){
            scanf("%d", &x);
            v[x].push_back(i);
        }
        for(int i=0;i<m;i++){
            scanf("%d %d", &o, &x);
            o--;
            if(o<v[x].size()){
                printf("%d\n", v[x][o]+1);
            }else{
                printf("0\n");
            }
        }
    }
    return EXIT_SUCCESS;
}
