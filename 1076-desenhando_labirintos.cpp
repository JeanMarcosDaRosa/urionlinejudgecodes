//Desenhando Labirintos
 
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <vector>
 
using namespace std;
 
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<bool> vb;
 
int cont = 0;
 
void dfs(int idx, vb &visitado, vvi &lab){
    cont+=2;
    visitado[idx]=true;
    int endIndex = lab[idx].size();
    for(int i=0; i<endIndex; i++){
        if(!visitado[lab[idx][i]])
            dfs(lab[idx][i], visitado, lab);
    }
}
 
int main(){
    int casos, n, v, a, ini, fim;
    scanf("%d", &casos);
    for(int i=0;i<casos;i++){
        scanf("%d %d %d", &n, &v, &a);
        vvi lab(v);
        vb visitado(v);
        cont=0;
        for(int j=0;j<a;j++){
            scanf("%d %d", &ini, &fim);
            lab[ini].push_back(fim);
            lab[fim].push_back(ini);
        }
        dfs(n, visitado, lab);
        printf("%d\n", cont-2);
    }
    return EXIT_SUCCESS;
}