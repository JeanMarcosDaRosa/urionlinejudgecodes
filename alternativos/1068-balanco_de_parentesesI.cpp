//Balanço de Parênteses I

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <stack>

using namespace std;

int main(){
    string in;
    while(getline(cin, in)){
        size_t tam = in.size();
        stack<char> pilha;
        bool correct = true;
        for(int i=0;i<tam;i++){
            if(in[i]=='(') pilha.push('('); else
            if(in[i]==')'){
                if(!pilha.empty()&&(pilha.top()=='(')){
                    pilha.pop();
                }else{
                    correct = false;
                    break;
                }
            }
        }
        if(!correct){
            cout << "incorrect\n";
        }else{
            if(pilha.empty()){
                cout << "correct\n";
            }else cout << "incorrect\n";
        }
    }
    return EXIT_SUCCESS;
}
