//A lenda de Flavious Josephus
#include <stdio.h>
#include <stdlib.h>


int josephus(int n, int k){
    if(n==1) return 1;
    return (josephus(n-1, k)+k-1)%n+1;
}

int main(){
    int c = 0, n, k;
    scanf("%d", &c);
    for(int i=0;i<c;i++){
        scanf("%d %d", &n, &k);
        printf("Case %d: %d\n", i+1,josephus(n, k));
    }
    return 0;
}
