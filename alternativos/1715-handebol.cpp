#include <iostream>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int main(){
	int p, j, cont = 0, x;
	cin >> j >> p;
	for(int i=0;i<j;i++){
		int nao_fez_gol = false;
		for(int k=0;k<p;k++){
			cin >> x;
			if (x<1){
				nao_fez_gol = true;
			}
		}
		if (!nao_fez_gol){
			cont++;
		}
	}
	printf("%d\n", cont);
	return 0;
}