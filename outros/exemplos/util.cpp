#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <time.h>
#define SIZE 4

using namespace std;

int main(){
    int vetSeq[SIZE];
    char vetId[2] = {'A','B'};
    do{
        for(int i=1;i<=SIZE;i++){
            vetSeq[i-1]=i;
        }
        do{
            bool j=false;
            for(int i=0;i<SIZE;i++, j=!j){
                cout << vetId[j] << vetSeq[i] << " ";
            }
            cout << "\n";
        }while(next_permutation(vetSeq, vetSeq+SIZE));
    }while(next_permutation(vetId, vetId+2));
    return EXIT_SUCCESS;
}
