#include <iostream>
#include <stdio.h>
using namespace std;

#define MAX 100000
#define MOD 1300031

typedef long long llong; //apenas para evitar de escrever 'long long' a cada declara��o

llong mod(llong x){
    return ((x%MOD)+MOD)%MOD;  //evita m�dulos negativos
}

int main(){
    llong A[MAX];
    int t, n;
    scanf("%d", &t);
    while (t--){
        scanf("%d", &n);
        llong P = 0, S = 0;
        for (int i = 0; i < n; ++i){
            scanf("%lld", &A[i]);
            P += A[i];
        }
        for (int i = 0; i < n; ++i){
            S = mod(S+mod(mod(P)*A[i]));
            P -= A[i];
        }
        printf("%lld\n", S);
    }
    return 0;
}
