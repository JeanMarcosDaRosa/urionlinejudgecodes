/**
    Sincronizar legendas

    O programa deve receber por parameto o que deseja mexer
    (
     H - hora
     M - minuto
     S - segundo
     MLS - milisegundo

     ), se é pra adiantar (A) ou retroceder (R) e a quantidade

    Portanto para funcionar devemos chamar o programa assim:
    legendas M A 1 para adiantar em um minuto a legenda.

    Para ter entrada usamos os operador de menor com o nome da
    legenda de entrada e o operador maior, escrevendo um nome
    da saida. Com isso as entradas e saidas serão as padrões.

    Ex:
        MLS A 340 < filme.srt > filmeFinal.str

        Com esta entrada será alterado os Milisegundos, adiantando-os
        340mls nos mls do filme.str e gravado a saída alterada no arq.
        filmeFinal.str

*/


#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#define TAM 200

void obtemString(char *str);
void modificaLegenda(char *campo, char *adiantarRetroceder, int quantidade);
void adiantaRetrocede(char *campo, char *adiantarRetroceder, int quantidade,
                      int hi, int mi, int si, int mili,
                      int hf, int mf, int sf, int milf);


int main(int argc, char *argv[])
{
    int i = 1;

    if(argc == 4)
    {
        ///Habilitando os dois prints abaixo, obtem-se os dados da entrada
        //printf("Numero de argumentos [%d]\n", argc);
        for(; i < argc; i++)
        {
            //printf("Argumento [%s]\n", argv[i]);
        }

        modificaLegenda(argv[1], argv[2], atoi(argv[3]));

    }
    else
    {
        printf("Por favor, verifique os argumentos\n");
        exit(1);
    }


    return 0;
}

///Função que trabalho diretamente no tempo da legenda
void adiantaRetrocede(char *campo, char *adiantarRetroceder, int quantidade,
                      int hi, int mi, int si, int mili,
                      int hf, int mf, int sf, int milf)
{
    if(strcmp(campo, "H") == 0)
    {
        if(strcmp(adiantarRetroceder, "A") == 0)
        {
            hi+=quantidade;
            hf+=quantidade;
            printf("%d:%d:%d,%d --> %d:%d:%d,%d", hi, mi, si, mili, hf, mf, sf, milf);
        }
        else
        {
            if(strcmp(adiantarRetroceder, "R") == 0)
            {
                if(hi-quantidade >= 0)
                {
                    hi-=quantidade;
                }
                if(hf-quantidade >= 0)
                {
                    hf-=quantidade;
                }
                printf("%d:%d:%d,%d --> %d:%d:%d,%d", hi, mi, si, mili, hf, mf, sf, milf);
            }
        }
    }
    else
    {
        if(strcmp(campo, "M") == 0)
        {
            if(strcmp(adiantarRetroceder, "A") == 0)
            {
                mi += quantidade;
                if(mi > 59)
                {
                    mi = mi - 60;
                    hi++;
                }

                mf += quantidade;
                if(mf > 59)
                {
                    mf = mf - 60;
                    hf++;
                }
                printf("%d:%d:%d,%d --> %d:%d:%d,%d", hi, mi, si, mili, hf, mf, sf, milf);
            }
            else
            {
                if(strcmp(adiantarRetroceder, "R") == 0)
                {
                    mi -=  quantidade;
                    if(mi < 0)
                    {
                        mi = 60 + mi;
                        hi--;
                        if(hi < 0)
                        {
                            exit(2);
                        }
                    }

                    mf -= quantidade;
                    if(mf<0)
                    {
                        mf = 60 + mf;
                        hf--;
                        if(hf < 0)
                        {
                            exit(2);
                        }
                    }
                }
                printf("%d:%d:%d,%d --> %d:%d:%d,%d", hi, mi, si, mili, hf, mf, sf, milf);
            }

        }
        else
        {
            if(strcmp(campo, "S") == 0)
            {
                if(strcmp(adiantarRetroceder, "A") == 0)
                {
                    si += quantidade;
                    if(si > 59)
                    {
                        si = si - 60;
                        mi++;
                        if(mi > 59)
                        {
                            mi = mi - 60;
                            hi++;
                        }
                    }

                    sf += quantidade;
                    if(sf > 59)
                    {
                        sf = sf - 60;
                        mf++;
                        if(mf > 59)
                        {
                            mf = mf - 60;
                            hf++;
                        }
                    }
                    printf("%d:%d:%d,%d --> %d:%d:%d,%d", hi, mi, si, mili, hf, mf, sf, milf);

                }
                else
                {
                    if(strcmp(adiantarRetroceder, "R") == 0)
                    {
                        si -= quantidade;
                        if(si < 0)
                        {
                            si = 60 + si;
                            mi--;
                            if(mi < 0)
                            {
                                mi = 60 + mi;
                                hi--;
                                if(hi < 0)
                                {
                                    exit(2);
                                }
                            }
                        }

                        sf -= quantidade;
                        if(sf < 0)
                        {
                            sf = 60 + sf;
                            mf--;
                            if(mf < 0)
                            {
                                mf = 60 + mf;
                                hf--;
                                if(hf < 0)
                                {
                                    exit(2);
                                }
                            }
                        }
                        printf("%d:%d:%d,%d --> %d:%d:%d,%d", hi, mi, si, mili, hf, mf, sf, milf);
                    }
                }
            }
            else
            {
                if(strcmp(campo, "MLS") == 0)
                {
                    if(strcmp(adiantarRetroceder, "A") == 0)
                    {
                        mili += quantidade;
                        if(mili > 999)
                        {
                            mili = mili - 1000;
                            si++;
                            if(si > 59)
                            {
                                si = si - 60;
                                mi++;
                                if(mi > 59)
                                {
                                    mi = mi - 60;
                                    hi++;
                                }
                            }
                        }

                        milf += quantidade;
                        if(milf > 999)
                        {
                            milf = milf - 1000;
                            sf++;
                            if(sf > 59)
                            {
                                sf = sf - 60;
                                mf++;
                                if(mf > 59)
                                {
                                    mf = mf - 60;
                                    hf++;
                                }
                            }
                        }
                        printf("%d:%d:%d,%d --> %d:%d:%d,%d", hi, mi, si, mili, hf, mf, sf, milf);
                    }
                    else
                    {
                        if(strcmp(adiantarRetroceder, "R") == 0)
                        {
                            mili -= quantidade;
                            if(mili < 0)
                            {
                                mili = 1000 + mili;
                                si--;
                                if(si < 0)
                                {
                                    si = 60 + si;
                                    mi--;
                                    if(mi < 0)
                                    {
                                        mi = 60 + mi;
                                        hi--;
                                        if(hi < 0)
                                        {
                                            exit(2);
                                        }
                                    }
                                }
                            }

                            milf -= quantidade;
                            if(milf < 0)
                            {
                                milf = 1000 + milf;
                                sf--;
                                if(sf < 0)
                                {
                                    sf = 60 + sf;
                                    mf--;
                                    if(mf < 0)
                                    {
                                        mf = 60 + mf;
                                        hf--;
                                        if(hf < 0)
                                        {
                                            exit(2);
                                        }
                                    }
                                }
                            }
                            printf("%d:%d:%d,%d --> %d:%d:%d,%d", hi, mi, si, mili, hf, mf, sf, milf);
                        }
                    }
                }
            }
        }
    }
}

///Extrai da legenda o que é necessário para adiantar e retroceder e
///envia para a função "adiantaRetrocede" para mexer no tempo da legenda
void modificaLegenda(char *campo, char *adiantarRetroceder, int quantidade)
{
    int hi, mi, si, mili, hf, mf, sf, milf, linha, cont = 1, flag = 0;
    char c[TAM];

    //printf("Campo[%s]\nAdiantar/Retroceder[%s]\nQuantidade[%d]\n",
           //campo, adiantarRetroceder, quantidade);

    while(1)
    {
        obtemString(c);
        if(strlen(c) == 0)
            break;

        if(isdigit(c[0]))
        {
            linha = atoi(c);

            if(linha == cont)
            {
                scanf("%d:%d:%d,%d --> %d:%d:%d,%d", &hi, &mi, &si, &mili, &hf, &mf, &sf, &milf);
                printf("%d\n", linha);
                adiantaRetrocede(campo, adiantarRetroceder, quantidade, hi, mi, si, mili, hf, mf, sf, milf);
                cont+=1;
            }
            else
            {
                printf("%s", c);
            }

        }
        else
        {
            printf("%s", c);
        }
    }
}

///Obtem String até que seja final de arquivo
///Muito similar ao getline do c++
void obtemString(char *str)
{
    int i = 0, t = 0;
    char a;

    while(scanf("%c", &a) != EOF)
    {
        if(a != '\n')
        {
            str[i] = a;
            i++;
        }
        else
        {
            if(a == '\n')
            {
                str[i] = '\n';
                i++;
            }
            break;
        }
    }
    str[i] = '\0';
}

        /***************************************************
        *       Fábio Dornel - GNU GPL 2013                *
        *                                                  *
        *                                                  *
        ****************************************************/
