#include <stdio.h>
#include <stdlib.h>     /* calloc, exit, free */



int main()
{
   int reg, pontes;
   while ( scanf( "%d %d", &reg, &pontes ) != EOF ) {
       int isPossible = false;
       int A, B;
       int* regioes = ( int* ) calloc( reg, sizeof( int ) );
       
       for ( int i=0; i < pontes; i++ ) {
           scanf( "%d %d", &A, &B );
           regioes[ A-1 ]++;
           regioes[ B-1 ]++;
       }
       
    	for (int i=0; i < reg; i++) {
             if(regioes[i] == pontes) 
                isPossible = true;
			for (int j=1; j < reg; j++) {
                 if(regioes[j] == pontes) 
                isPossible = true;
				if ( (regioes[i] + regioes[j]) == pontes )
				{
                    //printf("isPossible %d com pontes %d", isPossible, pontes);
					isPossible = true;
                    break;
				}
			}
            if (isPossible) break;
		}
        
       
	    if (isPossible)
            printf("S\n");
        else
            printf("N\n");
            
       free(regioes);
   }
}
