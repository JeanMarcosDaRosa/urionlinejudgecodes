/* Solu??o de Leandro Zatesko */

#include<stdio.h>

int expmod(int a, int x, int n) {
	unsigned long long r;
	if (x == 0) return 1 % n;
	if (x % 2) {
		r = (unsigned long long)a * expmod(a, x-1, n) % n;
		return (int)r;
	}
	r = expmod(a, x/2, n);
	r = r * r % n;
	return (int)r;
}

int main(void) {
	int a, n;
	scanf("%d %d", &a, &n);
	while (a) {
		printf("%d\n", expmod(a, n-1, n));
		scanf("%d %d", &a, &n);
	}
	return 0;
}

