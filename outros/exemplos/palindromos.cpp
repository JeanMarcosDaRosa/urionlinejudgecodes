#include <stdio.h>
#include <string.h>
#include <iostream>
#include <ctype.h>
using namespace std;

int main()
{
    string a;

    while(getline(cin, a))
    {
        int i = 0, j, flag = 0;
        j = a.size()-1;

        for(; i< j;i++, j--)
        {
            while(!isalpha(a[i]) && !isdigit(a[i]) && i<a.size()) {
                i++;
            }
            while(!isalpha(a[j]) && !isdigit(a[j]) && j>=0) {
                j--;
            }
//            printf("(i:%d) %c | %c (j:%d)\n", i, tolower(a[i]), tolower(a[j]), j);
            if(tolower(a[i]) != tolower(a[j]))
            {
                flag = 1;
                break;
            }

        }

        if(flag == 0)
            printf("%s\n", a.c_str());

    }
    return 0;
}

