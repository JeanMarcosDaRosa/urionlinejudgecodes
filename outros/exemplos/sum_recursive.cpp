#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <algorithm>

using namespace std;

int compare (const void * a, const void * b){
  return ( *(int*)b - *(int*)a );
}

bool rec_sum(int *v, int p, int s, int sum, int k){
    if(sum==k&&(sum>0)){
        return true;
    }
    if(p>=s||sum>k){
        return false;
    }
    for(int i=p;i<s;i++){
        if(rec_sum(v, i+1, s, sum+v[i], k)) return true;
    }
    return false;
}

void combine(int *vet, int tam_v, int k){
    int i=0;
    while(i<tam_v&&vet[i]>k)i++;
    cout << (rec_sum(vet, i, tam_v, 0, k)?"S\n":"N\n");//vetor, posicao inicial, tamanho, soma
}

int main(){
    int vet[] = {4,2,3,3,4,2};
    int s = sizeof(vet)/sizeof(int);
    qsort(&vet[0], s, sizeof(int), compare);
    for(int i=0;i<20;i++){
        combine(vet, s, i);
    }
    return EXIT_SUCCESS;
}
