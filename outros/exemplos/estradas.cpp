/* Solu??o de Neilor Tonin */
/* C Program On Krushkal's Algorithm */
#include <iostream>
#include <cstdio>
#include <algorithm>

using namespace std;

typedef struct {
    int origem;
    int destino;
    int peso;
} edge;

bool compara( edge A, edge B) {
    return (A.peso < B.peso);
}

int checkcycle(int p[],int i,int j) {
    // cout << "de "<< i << " para " << j << endl;
    while (p[i] != -1) {
        //     cout << "i"<< i << endl;
        i = p[i];
    }
    while (p[j] != -1) {
        //     cout << j << endl;
        j = p[j];
    }
    if (i!=j) {
        p[j]=i;
        return 1;
    }
    return 0;
}

int main() {
    edge e[100000];
    int parent[100000];
    int n,i,j,m,k,cost,NC,building;
    int origem,destino,peso;

    scanf("%d %d", &n, &m);

    while (n) {
        cost=0;
        for (i=0; i< n; i++) {
            parent[i]=-1;
        }
        int somatotal=0;
        for (i=0; i< m; i++) {
            scanf("%d %d %d", &e[i].origem,&e[i].destino,&e[i].peso);
            somatotal+=e[i].peso;
        }
        // cout << somatotal;
        int arestas=i;
        sort (e, e+arestas, compara);
        i = 0;
        k = 1;
        while (k < n && arestas>i ) {//(k< n) i < m{
            if (checkcycle(parent,e[i].origem,e[i].destino)) {
                k++;
                cost=cost+e[i].peso;
            }
            i++;
        }
        printf("%d\n", somatotal-cost);
        scanf("%d %d", &n, &m);
    }
    return 0;
}

