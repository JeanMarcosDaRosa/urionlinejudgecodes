#include <iostream>
#include <map>
#include <vector>
#include <algorithm>

using namespace std;

bool ordena(map<string, float>::iterator a, map<string, float>::iterator b)
{
    return a->second < b->second;
}

int main()
{
    map<string, float> notas;

// Inserir
    notas["eu"] = 9.0;
    notas["tu"] = 10;
    notas["ele"] = 8.8;

// consultar
    string s;
    cout << "Nome do aluno:" ;
    getline(cin, s);
    if(notas.find(s)!=notas.end()){
        notas[s] -= 0.5;
        cout << "Nota: " << notas[s] << '\n';
    }
    else cout << "nao achou\n";

    cout << "\n\nMostrar todos (ordem da chave)\n";
    // percorrer
    for(map<string, float>::iterator it=notas.begin(); it!=notas.end(); ++it)
        cout << it->first << ' ' << it->second << endl;

    cout << "Mostrar em ordem reversa  (ordem da chave)\n";
    // percorrer
    for(map<string, float>::reverse_iterator it=notas.rbegin(); it!=notas.rend(); ++it)
        cout << it->first << ' ' << it->second << endl;

    cout << "Mostrar em ordem  da nota\n";
    // Cria um VIO  (vetor indireto de ordena??o)
    vector<map<string, float>::iterator> vio;
    for(map<string, float>::iterator it=notas.begin(); it!=notas.end(); ++it)
        vio.push_back(it);

    // Ordena o VIO
    sort(vio.begin(), vio.end(), ordena);
    // Exibe indiretamente
    for(vector<map<string, float>::iterator>::iterator vit=vio.begin(); vit!=vio.end(); ++vit)
        cout << (*vit)->first << ' ' << (*vit)->second << endl;

    return 0;
} 
