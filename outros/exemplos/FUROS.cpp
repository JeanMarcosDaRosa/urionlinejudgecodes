#include <cstdio>
#include <cmath>

using namespace std;

#define MAXN 1000 //n�mero m�ximo de pontos na entrada

int pontos[MAXN][2]; //pontos[i][0] = a posi��o X do i-�simo ponto, pontos[i][1] a posi��o y
int N; //n�mero de pontos

//calcula a dist�ncia entre os pontos i e j da matriz pontos
double calcular_distancia(int i, int j) {
    return sqrt(pow(pontos[i][0] - pontos[j][0], 2) + pow(pontos[i][1] - pontos[j][1], 2));
}

//calcula e retorna a resposta para a inst�ncia atual do problema
double menor_diametro() {

    double menor_raio = INFINITY; //raio do menor c�rculo encontrado que cobriria todos os furos

    for(int i = 0; i < N; i++) {
    double maior_distancia = 0;

    for(int j = 0; j < N; j++) {

        double d = calcular_distancia(i, j);

        if(d > maior_distancia) {
        maior_distancia = d;
        }
    }

    if(maior_distancia < menor_raio) {
        menor_raio = maior_distancia;
    }
    }

    return menor_raio * 2 + 5;
}

int main() {

    int casoTeste = 1;

    while(true) {

    scanf("%d", &N);

    if(N == 0) //fim da entrada
        break;

    for(int i = 0; i < N; i++) {
        scanf("%d %d", &pontos[i][0], &pontos[i][1]);
    }

    printf("Teste %d\n", casoTeste);
    printf("%.0lf\n\n", menor_diametro());

    casoTeste++;
    }

    return 0;
}
