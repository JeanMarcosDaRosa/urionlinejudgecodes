#include <iostream>
#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;

int main(int argc, char *argv[]) {
    int n;
    long long int m;
    while(scanf("%d %lld", &n, &m) != EOF) {
        int vet[n];
        for(int i=0; i<n; i++) vet[i]=0;
        for(long long int i=0;i<m;i++){
            char cmd;
            int ini, fim;
            scanf(" %c %d %d", &cmd, &ini, &fim);
            if(cmd=='M'){
                for(int k=ini-1;k<=fim-1;k++){
                    if(vet[k]==0 || vet[k]==1 ) vet[k]++; else vet[k]=0;
                }
            }else if(cmd=='C'){
                int homem=0, elefante=0, rato=0;
                for(int k=ini-1;k<=fim-1;k++){
                    if(vet[k]==0) homem++; else
                    if(vet[k]==1) elefante++; else
                    if(vet[k]==2) rato++;
                }
                printf("%d %d %d\n", homem, elefante, rato);
            }
        }
        printf("\n");
    }
}
