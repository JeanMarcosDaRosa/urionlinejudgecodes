#include <iostream>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <map>
#define ii pair<int, int>
using namespace std;

int main(int argc, char *argv[]) {
    int a, c;
    while (cin >> a >> c) {
        int tam = a;
        map<int, int> placas;

        int grana = 0;
        for (int i = 0; i < c; i++) {
            char k;
            int placa, sz;
            cin.ignore();
            cin >> k;
            if (k == 'C') {
                cin >> placa >> sz;
                if ((tam - sz) < 0) continue;

                placas[placa] = sz;
                tam -= sz;
                grana += 10;
            } else {
                cin >> placa;
                tam += placas[placa];
            }
        }
        cout << grana << endl;
    }
}
