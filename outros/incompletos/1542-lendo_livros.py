# 1542 - Lendo Livros

while True:
	try:
		a, k, b = tuple([int(y) for y in input().split(' ')])
		c = 1
		diff = a * k
		j = b
		while (b*c) - (a*c) < diff:
			j += b
			c += 1
		print(j, 'paginas' if j > 1 else 'pagina')
	except:
		break