//Acampamento de F�rias

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <vector>

using namespace std;

typedef struct{
    int idade;
    string nome;
} pessoa;

int main(){
    int grupo, idx, idx_t;
    string nome;
    while(scanf("%d ", &grupo)&&grupo){
        vector<pessoa> vet(grupo);
        for(int i=0;i<grupo;i++){
            cin >> vet[i].nome;
            cin >> vet[i].idade;
        }
        idx=((vet[0].idade) % grupo);
        for(int i=0;i<grupo-1;i++){
            idx_t = ((vet[idx].idade+idx) % (grupo-i));
            if(vet[idx].idade%2==0){
                idx_t = (grupo-i)-idx_t;
            }
            cout << idx << ": " << vet[idx].nome << ", ";
            vet.erase(vet.begin()+idx);
            idx = idx_t-1;
        }
        cout << endl << "Vencedor(a): " << vet[0].nome << "\n";
    }
    return EXIT_SUCCESS;
}
