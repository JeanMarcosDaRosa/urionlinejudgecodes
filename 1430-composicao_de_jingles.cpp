//Composição de Jingles

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    string jingle;
    while(getline(cin, jingle)&&jingle[0]!='*'){
        size_t t = jingle.size();
        int total = 0;
        float comp = 0;
        for(int i=1;i<t;i++){
            switch(jingle[i]){
                case 'W':
                    comp+=1;
                    break;
                case 'H':
                    comp+=1.0/2;
                    break;
                case 'Q':
                    comp+=1.0/4;
                    break;
                case 'E':
                    comp+=1.0/8;
                    break;
                case 'S':
                    comp+=1.0/16;
                    break;
                case 'T':
                    comp+=1.0/32;
                    break;
                case 'X':
                    comp+=1.0/64;
                    break;
                case '/':
                    if(comp==1) total++;
                    comp=0;
                    break;
            }
        }
        printf("%d\n", total);
    }
    return EXIT_SUCCESS;
}
