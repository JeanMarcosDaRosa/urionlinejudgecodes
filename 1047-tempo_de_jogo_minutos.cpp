//Tempo de Jogo com Minutos
#include <stdio.h>
#include <stdlib.h>

int main(){
    int hi, hf, mi, mf, h, m;
    scanf("%d %d %d %d", &hi, &mi, &hf, &mf);
    if(hi>hf){
        h = (24-hi)+hf;
    }else{
        h = hf-hi;
    }
    if(mf>=mi){
        m = mf-mi;
    }else{
        m = (60-mi)+mf;
        h--;
    }
    printf("O JOGO DUROU %d HORA(S) E %d MINUTO(S)\n",h, m);
    return 0;
}