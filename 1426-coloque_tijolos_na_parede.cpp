//Coloque Tijolos na Parede

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <vector>

using namespace std;

int main(){
    int casos, x;
    scanf("%d", &casos);
    for(int i=0;i<casos;i++){
        int m[10][10];
        for(int l=0;l<9;l+=2){
            for(int c=0;c<=l;c+=2){
                scanf("%d", &m[l][c]);
            }
        }
        for(int x=1;x<9;x+=2) m[8][x]=(m[6][x-1]-m[8][x-1]-m[8][x+1])/2;
        for(int l=7;l>0;l--){
            for(int c=0;c<=l;c++){
                m[l][c]=m[l+1][c]+m[l+1][c+1];
            }
        }
        for(int l=0;l<9;l++){
            printf("%d", m[l][0]);
            for(int c=1;c<=l;c++){
                printf(" %d", m[l][c]);
            }
            printf("\n");
        }
    }
    return EXIT_SUCCESS;
}
