#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    int casos, q, x, m, z;
    scanf("%d", &casos);
    for(int i=0;i<casos;i++){
        scanf("%d", &q);
        m=q/2;
        for(int j=0;j<q;j++){
            scanf("%d", &x);
            if(j==m) z=x;
        }
        printf("Case %d: %d\n", i+1, z);
    }
    return EXIT_SUCCESS;
}
