//1074 - Par ou �mpar
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    int k, i, n;
    scanf("%d", &k);
    for(i=0;i<k;i++){
        scanf("%d", &n);
        if(n==0){
            printf("NULL\n");
        }else{
            if(n%2==0){
                printf("EVEN");
            }else {
                printf("ODD");
            }
            if(n>0){
                printf(" POSITIVE\n");
            }else{
                printf(" NEGATIVE\n");
            }
        }
    }
    return 0;
}
