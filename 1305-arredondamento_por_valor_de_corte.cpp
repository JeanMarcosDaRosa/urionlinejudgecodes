//Arredondamento por Valor de Corte

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <math.h>
using namespace std;

int main(){
    double num, corte, resto;
    while(scanf("%lf", &num)!=EOF){
        scanf("%lf", &corte);
        resto = num - (floor(num));
        if(resto<corte){
            printf("%.0lf\n", floor(num));
        }else{
            printf("%.0lf\n", ceil(num));
        }
    }
    return EXIT_SUCCESS;
}
