//Schweisen

#include <iostream>
#include <stdio.h>
#include <map>
#include <algorithm>

using namespace std;

typedef pair<unsigned short int, unsigned short int> par;

int main(){
    int x, y, p, q, a1, b1, a2, b2, val;
    int cont, lin_min, lin_max, col_min, col_max;
    char cmd;
    map<par, int>::iterator it;
    while((scanf("%d %d %d", &x, &y, &p))&&!(x==0&&y==0&&p==0)){
        scanf("%d", &q);
        getchar();
        map<par, int> matriz;
        for(int i=0;i<q;i++){
            scanf("%c", &cmd);
            if(cmd=='A'){
                scanf("%d %d %d", &val, &a1, &b1);
                getchar();
                matriz[make_pair(a1, b1)]+=val;
            }else if(cmd=='P'){
                cont=0;
                scanf("%d %d %d %d", &a1, &b1, &a2, &b2);
                getchar();
                lin_min = min(a1,a2);
                lin_max = max(a1,a2);
                col_min = min(b1,b2);
                col_max = max(b1,b2);
                for(it=matriz.lower_bound(make_pair(lin_min, col_min)); it!=matriz.end(); it++){
                //for(it=matriz.begin(); it!=matriz.end(); it++){
                    if((it->first.first>=lin_min && it->first.first<=lin_max) &&
                        (it->first.second>=col_min && it->first.second<=col_max)){
                        cont+=it->second;
                    }
                    if (it->first.first>lin_max && it->first.second>col_max) break;
                }
                cout << cont*p << "\n";
            }
        }
        cout << "\n";
    }
    return 0;
}
