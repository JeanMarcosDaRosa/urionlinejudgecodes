//Cara ou coroa
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

bool verificaDiagonal(int x, int y, int x2, int y2){
    int i=x, j=y;
    while(i<8&&j<8){
        i++;
        j++;
        if(i==x2&&j==y2){
            return true;
        }
    }
    i=x;
    j=y;
    while(i>0&&j>0){
        i--;
        j--;
        if(i==x2&&j==y2){
            return true;
        }
    }
    i=x;
    j=y;
    while(i>0&&j<8){
        i--;
        j++;
        if(i==x2&&j==y2){
            return true;
        }
    }
    i=x;
    j=y;
    while(i<8&&j>0){
        i++;
        j--;
        if(i==x2&&j==y2){
            return true;
        }
    }
    return false;
}

int main()
{
   int x, y, x2, y2;
   while(scanf("%d %d %d %d", &x, &y, &x2, &y2)&&(x!=0||y!=0||x2!=0||y2!=0)){
       if(x==x2&&y==y2){
           printf("0\n");
       }else if((x==x2||y==y2)||verificaDiagonal(x, y, x2, y2)){
           printf("1\n");
       }else printf("2\n");
   }
   return 0;
}
