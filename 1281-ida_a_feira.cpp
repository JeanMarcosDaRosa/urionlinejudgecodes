//Ida � Feira

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <map>

using namespace std;

int main(){
    int casos;
    scanf("%d", &casos);
    for(int i=0;i<casos;i++){
        int disp, qtdCompra, qtdFruta;
        string fruta;
        double preco, total=0.0;
        map<string,double> prateleira, cesto;
        scanf("%d", &disp);
        for(int j=0;j<disp;j++){
            cin >> fruta;
            scanf("%lf", &preco);
            prateleira[fruta] = preco;
        }
        scanf("%d", &qtdCompra);
        for(int j=0;j<qtdCompra;j++){
            cin >> fruta;
            scanf("%d", &qtdFruta);
            if(cesto.find(fruta)!=cesto.end()){
                cesto[fruta]+=qtdFruta;
            }else{
                cesto[fruta]=qtdFruta;
            }
        }
        for(map<string, double>::iterator it=cesto.begin();it!=cesto.end();++it){
            total+=((double) (it->second))*prateleira[it->first];
        }
        printf("R$ %.2lf\n",total);
    }
    return EXIT_SUCCESS;
}
