//Lajotas Hexagonais

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    int fib[41], x; fib[0] = fib[1] = 1;
    for(int i=2;i<=40;i++) fib[i] = fib[i-1]+fib[i-2];
    while(scanf("%d", &x)&&x!=0){
        printf("%d\n", fib[x]);
    }
    return EXIT_SUCCESS;
}
