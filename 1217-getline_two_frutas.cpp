//Getline Two - Frutas

#include <iostream>
#include <stdio.h>
#include <sstream>
#include <string.h>
using namespace std;

int main(){
    string in, aux;
    int c;
    double valor, tot=0, qtdKQ=0;
    scanf("%d", &c);
    for(int i=0; i<c;i++){
        int q=0;
        scanf("%lf ", &valor);
        tot+=valor;
        getline(cin, in);
        stringstream ss(in);
        while(ss>>aux)q++;
        qtdKQ+=q;
        printf("day %d: %d kg\n", i+1, q);
    }
    printf("%.2lf kg by day\n", qtdKQ/c);
    printf("R$ %.2lf by day\n", tot/c);
    return 0;
}
