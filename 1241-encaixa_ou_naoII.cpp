//Encaixa ou Nao II

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>

using namespace std;

int main(){
    int c;
    cin >> c;
    for(int i=0;i<c;i++){
        string a, b;
        cin >> a;
        cin >> b;
        if(a.size()<b.size()){
            cout << "nao encaixa\n";
        }else{
            bool diff=false;
            int id_a = a.size()-1;
            for(int i=b.size()-1;i>=0;i--, id_a--){
                if(a[id_a]!=b[i]){
                    diff = true;
                    break;
                }
            }
            if(diff){
                cout << "nao encaixa\n";
            }else{
                cout << "encaixa\n";
            }
        }
    }
    return 0;
}
