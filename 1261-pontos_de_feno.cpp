//Pontos de Feno

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <string>
#include <map>

using namespace std;

int main(){
    int dic, cases;
    string in;
    double val = 0;
    map<string, double> mapa;
    scanf("%d %d ", &dic, &cases);
    for(int i=0;i<dic;i++){
        cin >> in;
        cin >> val;
        mapa[in] = val;
    }
    for(int i=0;i<cases;i++){
        double tot = 0.0;
        while((cin >> in)&&in!="."){
            if(mapa.find(in)!=mapa.end()){
                tot+=mapa[in];
            }
        }
        printf("%.0lf\n", tot);
    }
    return EXIT_SUCCESS;
}
