//Acima da M�dia

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    int c;
    scanf("%d", &c);
    for(int i=0;i<c;i++){
        int qtd;
        double tot=0.0, qtdAcima=0.0;
        scanf("%d", &qtd);
        int turma[qtd];
        for(int j=0;j<qtd;j++){
            scanf("%d", &turma[j]);
            tot+=turma[j];
        }
        tot=tot/qtd;
        for(int j=0;j<qtd;j++){
            if(turma[j]>tot){
                qtdAcima+=1;
            }
        }
        printf("%.3lf%%\n", (qtdAcima/qtd)*100);
    }
    return EXIT_SUCCESS;
}
