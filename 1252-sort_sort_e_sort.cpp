//Sort! Sort!! e Sort!!!

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <map>
#include <stdio.h>
#include <algorithm>

using namespace std;

bool compare(const int a, const int b){
    if((a%2==0)&&(b%2==0)){
        return a<b;
    }
    if((a%2!=0)&&(b%2!=0)){
        return b<a;
    }
    if((a%2!=0)&&(b%2==0)){
        return true;
    }
    if((a%2==0)&&(b%2!=0)){
        return false;
    }
}

int main() {
    int n, val, m;
    while(scanf("%d %d", &n, &m)){
        cout << n << " " << m << "\n";
        if(n==0 && m==0) break;
        map<int, vector<int> > mapa;

        for (int i=0; i<n; i++) {
            scanf("%d", &val);
            mapa[val % m].push_back(val);
        }

        for(map<int, vector<int> >::iterator it=mapa.begin(); it!=mapa.end(); ++it){
            sort(it->second.begin(), it->second.end(), compare);
            for(vector<int>::iterator it2=it->second.begin(); it2!=it->second.end(); ++it2){
                cout << *it2 << "\n";
            }
        }

    }
    return EXIT_SUCCESS;
}


/*
int m;

int cmp(const void *a, const void *b){
    int ma = a%m, mb = b%m;
}

int main(){
    int n, val;
    while(scanf("%d %d", &n, &m)){
        cout << n << " " << m << "\n";
        if(n==0&&m==0) break;
        vector<int> vet;
        for(int i=0;i<n;i++){
            scanf("%d", &val);
            vet.push_back(val);
        }
        sort(vet.begin(), vet.end(), cmp);
    }
    return EXIT_SUCCESS;
}
*/
