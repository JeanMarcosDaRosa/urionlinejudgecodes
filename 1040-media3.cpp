//Media 3
#include <stdio.h>
#include <stdlib.h>

int main(){
    float a, b, c, d, media, exame;
    scanf("%f %f %f %f", &a, &b, &c, &d);
    media = (a*0.2)+(b*0.3)+(c*0.4)+(d*0.1);
    printf("Media: %.1f\n",media);
    if(media>=7.0){
        printf("Aluno aprovado.\n");
    }else if(media<5.0){
        printf("Aluno reprovado.\n");
    } else{
        printf("Aluno em exame.\n");
        scanf("%f", &exame);
        printf("Nota do exame: %.1f\n", exame);
        media = (media+exame)/2.0;
        if(media>=5.0){
            printf("Aluno aprovado.\n");
        }else if(media<5.0){
            printf("Aluno reprovado.\n");
        }
        printf("Media final: %.1f\n", media);
    }
    return 0;
}