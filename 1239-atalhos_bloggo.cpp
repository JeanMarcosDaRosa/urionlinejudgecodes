#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <string.h>

using namespace std;

int main(){
    string in;
    while(getline(cin, in)){
        bool i=false, b=false;
        string aux;
        unsigned pos = in.find('*');
        while(pos!=std::string::npos){
            if(b)in.replace(pos, 1, "</b>");
            else in.replace(pos, 1, "<b>");
            b=!b;
            pos = in.find('*');
        }
        pos = in.find('_');
        while(pos!=std::string::npos){
            if(i)in.replace(pos, 1, "</i>");
            else in.replace(pos, 1, "<i>");
            i=!i;
            pos = in.find('_');
        }
        cout << in << "\n";
    }
    return EXIT_SUCCESS;
}
