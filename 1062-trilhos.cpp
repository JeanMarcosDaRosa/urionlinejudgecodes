//Trilhos
#include <iostream>
#include <stdio.h>
#include <stack>

using namespace std;

int main()
{
    int c;
    while(scanf("%d ", &c)&&c!=0){
        int x;
        while(scanf("%d ", &x)&&x!=0){
            stack<int> in, out;
            in.push(x);
            for(int i=0;i<c-1;i++){
                scanf("%d ", &x); 
                in.push(x);
            }
            int seq = in.size();
            while(!in.empty()){
                if(in.top()!=seq){
                    out.push(in.top());
                    in.pop();
                }else{
                    in.pop();
                    seq--;
                    while(!out.empty()&&out.top()==seq){
                        out.pop();
                        seq--;
                    }
                    
                }
            }
            if(in.empty()&&out.empty()){
                cout << "Yes\n";
            }else{
                cout << "No\n";
            }
        }
        cout << "\n";
        
    }
   
    return 0;
}

