//Lanche
#include "stdio.h"
#include "stdlib.h"

int main(){
    int c, q;
    float t[5] = {4.0,4.5,5.0,2.0,1.5};
    scanf("%d %d", &c, &q);
    printf("Total: R$ %.2f\n", q*t[c-1]);
    return 0;
}