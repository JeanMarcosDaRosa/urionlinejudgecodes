//Telefone Sem Fio

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    int casos;
    string correto, time1, time2;
    scanf("%d ", &casos);
    for(int i=0;i<casos;i++){
        getline(cin, correto);
        getline(cin, time1);
        getline(cin, time2);
        size_t s = correto.size();
        int t1=0, t2=0;
        short desempate = 0;
        for(int c=0;c<s;c++){
            if(correto[c]==time1[c]) t1++;
            if(correto[c]==time2[c]) t2++;
            if((desempate==0)&&(t1!=t2)){
                if(t1>t2) desempate = 1; else desempate = 2;
            }
        }
        printf("Instancia %d\n", i+1);
        if(t1==t2){
            if(desempate!=0) printf("time %d\n", desempate); else
            printf("empate\n");
        }else{
            if(t1>t2) printf("time 1\n"); else
            if(t1<t2) printf("time 2\n");
        }
        printf("\n");
    }
    return EXIT_SUCCESS;
}
