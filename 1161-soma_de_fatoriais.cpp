//Soma de Fatoriais
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int a, b;
    unsigned long long soma, fat;
    while(scanf("%d %d", &a, &b)!=EOF){
        fat=1;
        for(int i=1;i<=a;i++){
            fat*=i;
        }
        soma = fat;
        fat=1;
        for(int i=1;i<=b;i++){
            fat*=i;
        }
        soma+=fat;
        printf("%llu\n", soma);
    }
    return 0;
}
