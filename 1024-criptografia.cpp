//Criptografia

#include <iostream>
#include <stdio.h>

using namespace std;

int main(){
    int c;
    cin >> c;
    cin.ignore();
    for(int i=0;i<c;i++){
        string in;
        getline(cin, in);
        char linha[in.size()];
        for(int k=in.size()-1,j=0; k>=0; k--,j++){
            int x = in[k];
            if(isalpha(in[k]))x+=3;
            if(k<(in.size()/2)+(in.size()%2))x-=1;
            linha[j]=x;
            printf("%c", linha[j]);
        }
        printf("\n");
    }
    return 0;
}
