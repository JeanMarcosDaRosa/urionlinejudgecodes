//Matriz Quadrada II

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    int n;
    while(scanf("%d", &n)&&n!=0){
        int m[n][n];
        for(int i=0;i<n;i++){
            for(int idx=i, x=1;idx<n;idx++, x++){
                m[idx][i] = x;
                m[i][idx] = x;
            }
        }
        for(int i = 0; i < n; i ++){
            for(int j = 0; j < n; j ++){
                printf("%3d", m[i][j]);
                if(j!=n-1) printf(" ");
            }
            printf("\n");
        }
        printf("\n");
    }
    return EXIT_SUCCESS;
}
