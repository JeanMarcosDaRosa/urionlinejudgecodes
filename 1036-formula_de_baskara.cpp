//F�rmula de Baskara
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    double a, b, c, r1, r2, raiz = 0;
    scanf("%lf %lf %lf",&a, &b, &c);
    raiz = sqrt(pow(b, 2)-4*a*c);
    if(raiz>=0&&(2*a!=0)){
        r1=(-b+raiz)/(2*a);
        r2=(-b-raiz)/(2*a);
        printf("R1 = %.5lf\nR2 = %.5lf\n", r1, r2);
    }else{
        printf("Impossivel calcular\n");
    }

    return 0;
}
