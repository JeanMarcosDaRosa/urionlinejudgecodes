//Preenchimento de Vetor I
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int v[10];
    int a;
    scanf("%d", &a);
    v[0]=a;
    for(int i=1;i<10;i++){
        v[i]=v[i-1]*2;
    }
    for(int i=0;i<10;i++){
        printf("N[%d] = %d\n",i, v[i]);
    }
    return 0;
}
