//Divisores I

#include <stdio.h>

int main()
{
    int x;
    scanf("%d", &x);
    for(int i=1;i<=(x/2)+1;i++){
        if(x%i==0){
            printf("%d\n", i);
        }
    }
    printf("%d\n", x);
    return 0;
}
