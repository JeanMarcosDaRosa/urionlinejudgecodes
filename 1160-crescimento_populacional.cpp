#include <stdio.h>
#include <stdlib.h>
 
void calcula(long int PA, long int PB, double G1, double G2);
 
int main(){
    int T, i;
    long int PA, PB;
    double G1, G2;
    scanf("%d", &T);
    for(i=0;i<T;i++){
        scanf("%ld", &PA);
        scanf("%ld", &PB);
        scanf("%lf", &G1);
        scanf("%lf", &G2);
        calcula(PA, PB, G1, G2);
    }
 
    return 0;
}
 
void calcula(long int PA, long int PB, double G1, double G2){
    int count = 0;
    while((PB>=PA)&&(count<=100)){
        PA = PA+(PA*(G1/100.00000));
        PB = PB+(PB*(G2/100.00000));
        count++;
    }
    if(count<=100){
        printf("%d anos.\n", count);
    }else{
        printf("Mais de 1 seculo.\n");
    }
