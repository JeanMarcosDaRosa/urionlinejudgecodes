//1163 - Angry Ducks

#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;
double const g=9.80665, pi=3.14159/180.0;
int main(){
    double altura;
    while(cin >> altura){
        int inicio, fim, casos;
        cin >> inicio >> fim >> casos;
        for(int i=0; i<casos; i++){
            double A, V, atinge;
            cin >> A >> V;
            atinge = (V*cos(A*pi)*(-2*V*sin(A*pi)-pow((4*V*V*sin(A*pi)*sin(A*pi)+8*g*altura),0.5)))/(-2*g);
            if(atinge >= inicio && atinge <= fim)
                cout << fixed << setprecision(5) << atinge << " -> DUCK" << endl;
            else
                cout << fixed << setprecision(5) << atinge << " -> NUCK" << endl;
        }
    }
    return 0;
}