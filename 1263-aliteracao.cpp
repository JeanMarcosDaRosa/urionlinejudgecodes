//Aliteração

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <string>
#include <sstream>

using namespace std;

int main(){
    string in, aux;
    while(getline(cin, in)){
        int count = 0;
        bool is_alig = false;
        stringstream ss(in);
        ss >> aux;
        char first = toupper(aux[0]);
        while(ss >> aux){
            if(toupper(aux[0])!=first){
                if(is_alig) count++;
                is_alig = false;
                first = toupper(aux[0]);
            }else{
                is_alig = true;
            }
        }
        if(is_alig) count++;
        cout << count <<"\n";
    }
    return EXIT_SUCCESS;
}
