//Diamantes e Areia

#include <iostream>
#include <string.h>
#include <stack>

using namespace std;

int main()
{
   int c;
   cin>>c;
   for(int i=0;i<c;i++){
       string in;
       int count=0;
       stack<char> a;
       cin >> in;
       for(int k=0;k<in.size();k++){
           if(in[k]=='<'){
                a.push(in[k]);
           }else if(in[k]=='>'&&!a.empty()){
                count++;
                a.pop();
           }
       }
       cout << count << "\n";
   }
   return 0;
}
