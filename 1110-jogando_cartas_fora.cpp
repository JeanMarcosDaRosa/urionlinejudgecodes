//Jogando Cartas Fora

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <list>

using namespace std;

int main(){
    int carta, c;
    while(scanf("%d", &carta)&&carta!=0){
        list<int> pilha;
        for(int i=1;i<=carta;i++){
            pilha.insert(pilha.end(),i);
        }
        cout << "Discarded cards: ";
        while(pilha.size()>1){
            cout << *pilha.begin();
            if(pilha.size()>2) cout << ", ";
            pilha.erase(pilha.begin());
            c=*pilha.begin();
            pilha.insert(pilha.end(),c);
            pilha.erase(pilha.begin());
        }
        cout << "\nRemaining card: " << *pilha.begin() << "\n";
    }
    return EXIT_SUCCESS;
}
