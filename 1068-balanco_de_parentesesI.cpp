//Balan�o de Par�nteses I

#include <iostream>
#include <string.h>
#include <stack>

using namespace std;

int main()
{
   string in;
   while(cin >> in){
       stack<char> a;
       for(int k=0;k<in.size();k++){
           if(in[k]=='('){
                a.push(in[k]);
           }else if(in[k]==')'){
               if(!a.empty()){
                   a.pop();
               }else{
                   a.push(in[k]);
                   break;
               }
           }
       }
       if(a.size()==0){
           cout << "correct\n";
       }else{
           cout << "incorrect\n";
       }
       
   }
   return 0;
}