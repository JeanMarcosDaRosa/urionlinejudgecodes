//Getline Three - Cal�ados

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <string.h>
#include <sstream>

using namespace std;

int main(){
    int c, i=1;
    string in;
    while(scanf("%d ", &c)!=EOF){
        int m=0, f=0;
        getline(cin, in);
        stringstream ss(in);
        int num;
        while(ss >> num){
            ss >> in;
            if(num==c){
                if(in=="F")f++;
                else
                if(in=="M") m++;
            }
        }
        if(i>1) cout << "\n";
        cout << "Caso " << i << ":\n";
        cout << "Pares Iguais: " << m+f << "\n";
        cout << "F: " << f << "\n";
        cout << "M: " << m << "\n";
        i++;
    }
    return EXIT_SUCCESS;
}
