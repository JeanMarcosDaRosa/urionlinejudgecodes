#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    int vet [5];
    int c= 0;
    while(scanf("%d", &c)&&c!=0){
        for(int k=0;k<c;k++){
            for(int i=0;i<5;i++){
                scanf("%d", &vet[i]);
            }
            int alternativa = -1;
            bool resp = false;
            for(int i=0;i<5;i++){
                if(vet[i]<=127){
                    resp = true;
                    if(alternativa==-1){
                        alternativa = i;
                    }else{
                        printf("*\n");
                        alternativa = -1;
                        break;
                    }
                }
            }
            if(alternativa>=0){
                if(alternativa==0)printf("A\n");
                if(alternativa==1)printf("B\n");
                if(alternativa==2)printf("C\n");
                if(alternativa==3)printf("D\n");
                if(alternativa==4)printf("E\n");
            }else if(!resp){
                printf("*\n");
            }
        }
    }
    return 0;
}
