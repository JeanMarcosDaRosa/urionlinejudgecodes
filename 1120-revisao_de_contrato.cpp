//Revis�o de Contrato

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <algorithm>
#include <sstream>

using namespace std;

string remove_zeros(string in){
    while(in[0]=='0'){
        in.erase(in.begin());
    }
    return in;
}

int main(){
    string nro;
    char rm;
    int value;
    while(true){
        cin >> rm >> nro;
        if(rm=='0'&&nro=="0") break;
        nro.erase(remove(nro.begin(), nro.end(), rm), nro.end());
        nro = remove_zeros(nro);
        if(nro.size()>0){
            cout << nro << "\n";
        }else cout <<"0\n";
    }
    return EXIT_SUCCESS;
}
