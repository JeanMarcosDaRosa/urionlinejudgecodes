//Area Superior

#include <stdio.h>

int main()
{
    int x = 5, k = 1;
    double v=0, c=0;
    char op='S';
    double m[12][12];
    scanf("%c%*c", &op);
    for(int i=0;i<12;i++){
        for(int j=0;j<12;j++){
            scanf("%lf", &m[i][j]);
        }
    }
    for(int i=0;i<12-6;i++){
        for(int j=i+1;j<(12-k);j++){
            c++;
            v+=m[i][j];
            //printf("m[%d][%d]\n",i, j);
        }
        k++;
    }
    if(op=='S'){
        printf("%.1lf\n", v);
    }else if(op=='M'){
        printf("%.1lf\n", v/c);
    }
    return 0;
}
