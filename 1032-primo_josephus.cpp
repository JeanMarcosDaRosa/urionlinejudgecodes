//Primo de Josephus

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <iostream>
#include <set>
#include <vector>

using namespace std;

set<int> primos;

void num_primo(int qtd){
    int a, flag=1, ult_primo = 1;
    float n;
    for(int i=0;i<qtd;i++){
        switch (ult_primo){
            case 1:
                ult_primo = 2;
                break;
            case 2:
                ult_primo = 3;
                break;
            default:
                while(1){
                    flag=1;
                    ult_primo+=2;
                    if(ult_primo>7) while(ult_primo%2==0||ult_primo%3==0||ult_primo%5==0||ult_primo%7==0) ult_primo++;
                    n=sqrt(ult_primo);
                    for(a=2;a<=n;a++){
                        if(ult_primo%a==0){
                            flag=0;
                            break;
                        }
                    }
                    if(flag) break;
                }
                break;
        }
        primos.insert(ult_primo);
    }
}

int main(){
    int n = 0;
    vector<int>::iterator it;
    set<int>::iterator np;
    num_primo(3502);

    while(scanf("%d", &n)&&n!=0){
        vector<int> circulo;
        for(int a=1;a<=n;a++) circulo.push_back(a);
        register int idx=0, ult_primo, count = 0, t=n;
        np=primos.begin();
        for(int i=0;i<n-1;i++){
            ult_primo=*np++;
            idx = (idx+ult_primo-1) % t;
            circulo.erase(circulo.begin()+idx);
            t--;
        }
        it=circulo.begin();
        cout << *it << "\n";
    }
    return 0;
}
