//Tipos de Triangulo
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <algorithm>

bool _cmp (int i,int j) { return (j<i); }

int main(){
    static const size_t v_size = 3;
    double v[v_size];
    scanf("%lf %lf %lf", &v[0], &v[1], &v[2]);
    std::sort(v,v+v_size, _cmp);
    /*
    se A > B+C, apresente a mensagem: NAO FORMA TRIANGULO
    se A2 = B2 + C2, apresente a mensagem: TRIANGULO RETANGULO
    se A2 > B2 + C2, apresente a mensagem: TRIANGULO OBTUSANGULO
    se A2 < B2 + C2, apresente a mensagem: TRIANGULO ACUTANGULO
    se os tres lados forem iguais, apresente a mensagem: TRIANGULO EQUILATERO
    se apenas dois dos lados forem iguais, apresente a mensagem: TRIANGULO ISOSCELES
    */
    //printf("%lf %lf %lf\n", v[0], v[1], v[2]);
    if(v[0]>=(v[1]+v[2])){
        printf("NAO FORMA TRIANGULO\n");
    }else{
        if(pow(v[0],2)==(pow(v[1],2)+pow(v[2],2))){
            printf("TRIANGULO RETANGULO\n");
        }
        if(pow(v[0],2)>(pow(v[1],2)+pow(v[2],2))){
            printf("TRIANGULO OBTUSANGULO\n");
        }
        if(pow(v[0],2)<(pow(v[1],2)+pow(v[2],2))){
            printf("TRIANGULO ACUTANGULO\n");
        }
        if(v[0]==v[1]&&v[0]==v[2]){
            printf("TRIANGULO EQUILATERO\n");
        }
        if((v[0]==v[1]&&v[1]!=v[2])||(v[0]==v[2]&&v[1]!=v[2])||(v[1]==v[2]&&v[0]!=v[1])){
            printf("TRIANGULO ISOSCELES\n");
        }
    }
    return 0;
}
