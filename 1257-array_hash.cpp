//Array Hash

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>

using namespace std;

int main(){
    int cases, qtd, size_in, c;
    string in;
    cin >> cases;
    for(int i=0;i<cases;i++){
        cin >> qtd;
        getchar();
        int hash_in = 0;
        for(int j=0;j<qtd;j++){
            cin >> in;
            size_in = in.size();
            for(int k=0;k<size_in;k++){
                hash_in+=(in[k]-65)+k+j;
            }
        }
        cout << hash_in << "\n";
    }
    return EXIT_SUCCESS;
}
