//Substituição em Vetor I
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

using namespace std;

int main()
{
    int v[10];
    for(int i=0;i<10;i++){
        scanf("%d", &v[i]);
        if(v[i]<=0)v[i]=1;
    }
    for(int i=0;i<10;i++){
        printf("X[%d] = %d\n",i, v[i]);
    }
    return 0;
}
