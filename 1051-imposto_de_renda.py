# 1051 - Imposto de Renda
__author__ = 'jean'

def imposto(valor):
    if valor <= 2000.00:
        return 'Isento'
    else:
        valor -= 2000.00
        if valor <= 1000.00:
            return valor * 0.08
        else:
            acum = (1000.00 * 0.08)
            valor -= 1000.00
            if valor <= 1500.00:
                return acum + (valor * 0.18)
            else:
                acum += (1500.00 * 0.18)
                valor -= 1500.00
                return acum + (valor * 0.28)

val = float(input())
ret = imposto(val)
if ret != 'Isento':
    print("R$ %.2f" % ret)
else:
    print(ret)
