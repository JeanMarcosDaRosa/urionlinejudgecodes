//M�ltiplos de 13
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>

using namespace std;

int main(){
    int x, y, s=0;
    scanf("%d %d", &x, &y);
    if(y<x) swap(x, y);
    for(int i=x;i<=y;i++){
        if(i%13!=0){
            s+=i;
        }
    }
    printf("%d\n", s);
    return 0;
}