//Preenchimento de Vetor IV

#include <stdio.h>
#define TAMV 5
#define TAME 15

int main()
{
    int imp[TAMV], par[TAMV];
    int x, i1=0, i2=0;
    for(int i=0;i<TAMV;i++){
        par[i]=0;
        imp[i]=0;
    }
    for(int i=0;i<TAME;i++){
        scanf("%d", &x);
        if(x%2==0){
            par[i1] = x;
            if(i1>=TAMV-1){
                for(int j=0;j<TAMV;j++){
                    printf("par[%d] = %d\n", j, par[j]);
                    par[j] = 0;
                }
                i1=0;
            }else i1++;
        }else{
            imp[i2] = x;
            if(i2>=TAMV-1){
                for(int j=0;j<TAMV;j++){
                    printf("impar[%d] = %d\n", j, imp[j]);
                    imp[j]=0;
                }
                i2=0;
            }else i2++;
        }
    }
    for(int i=0;i<i2;i++){
        printf("impar[%d] = %d\n", i, imp[i]); 
    }
    for(int i=0;i<i1;i++){
        printf("par[%d] = %d\n", i, par[i]); 
    }
   return 0;
}
