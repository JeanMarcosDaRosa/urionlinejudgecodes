#include <stdio.h>
#include <algorithm>
using namespace std;
 
int bubble_sort(int *vet, int tam)
{
    int flag, count = 0;
 
    while(true)
    {
        flag = 0;
        for(int j = 0; j < tam-1; j++)
        {
            if(vet[j]>vet[j+1])
            {
                swap(vet[j], vet[j+1]);
                count++;
                flag = 1;
            }
        }
        if (!flag) break;
    }
 
    return count;
}
 
int main()
{
    int cases =0;
    int tam;
    scanf("%d", &cases);
 
    for (int i = 0; i < cases; i++)
    {
        scanf("%d", &tam);
        int vet[tam];
 
        for (int k=0; k<tam; k++)
        {
            scanf("%d", &vet[k]);
        }
 
        printf("Optimal train swapping takes %d swaps.\n", bubble_sort(vet, tam));
    }
 
    return 0;
