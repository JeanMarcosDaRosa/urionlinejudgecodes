//Matriz Quadrada I
#include "stdio.h"
#include "stdlib.h"

int **alocar_matriz (int m, int n){
    int **v;
    int   i;

    v = (int **) calloc (m, sizeof(int *));
    if (v == NULL) {
        printf ("Memoria insuficiente");
        return (NULL);
    }

    for ( i = 0; i < m; i++ ) {
        v[i] = (int*) calloc (n, sizeof(int));
        if (v[i] == NULL) {
            printf ("Memoria insuficiente");
            return (NULL);
        }
    }
    return (v);
}

int main(){
    int **matriz;
    int i, j, k, l, tam;
    
    scanf("%d", &tam);
    while(tam>0){
        matriz = alocar_matriz(tam, tam);
        for(i = 0; i < tam; i ++){
            for(j = 0; j < tam; j ++){
                matriz[i][j] = 1;
            }
        }
        l = tam;
        for(k = 1; k < l; k ++){
            for(i = k; i < (l-1); i ++){
                for(j = k; j < (l-1); j ++){
                    matriz[i][j] = k + 1;
                }
            }
            l--;
        }
        for(i = 0; i < tam; i ++){
            for(j = 0; j < tam; j ++){
                printf("%3d", matriz[i][j]);
                if(j!=tam-1) printf(" ");
            }
            printf("\n");
        }
        printf("\n");
        free(matriz);
        scanf("%d", &tam);
    }

   return 0;
}