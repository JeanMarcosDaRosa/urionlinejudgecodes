#include <stdio.h>
#include <map>
#include <string>
#include <string.h>

using namespace std;
int main()
{
    int casos, tot=0;
    map<string, int> trees;
    scanf("%d", &casos);
    getc(stdin); // limpa o buffer
    getc(stdin);
    for (int i=0; i<casos; i++)
    {
        char tree[31];

        while (true)
        {
            if (gets(tree)==NULL) break;
            if (strlen(tree)==0) break;

            if (trees.find(tree)==trees.end())
            {
                trees[tree] = 1;
            }
            else
            {
                trees[tree]++;
            }

            ++tot;
        }
        if (trees.size()==0) continue;

        for (map<string, int>::iterator it = trees.begin(); it!=trees.end(); it++)
        {
            double qt = (double) (100*it->second) / tot;
            string arv = it->first;
            printf("%s %.4f\n", &arv[0], qt);
        }
        if(i<casos-1)printf("\n");
        trees.clear();
        tot=0;
    }
}
