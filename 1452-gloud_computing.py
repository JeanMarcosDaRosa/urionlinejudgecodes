# Gloud Computing
__author__ = 'jean'

while True:
    conexoes = 0
    inp = input().split(' ')
    n, m = int(inp[0]), int(inp[1])
    if n == m == 0:
        break
    app = {}
    for i in range(0, n):
        inp = input().split(' ')
        for x in range(1, int(inp[0])+1):
            if inp[x] in app:
                app[inp[x]].append(i)
            else:
                app[inp[x]] = [i]
    for i in range(0, m):
        inp = input().split(' ')
        l_serv = set([])
        for x in range(1, int(inp[0])+1):
            if inp[x] in app:
                for s in app[inp[x]]:
                    l_serv.add(s)
        conexoes += len(l_serv)
    print(conexoes)
