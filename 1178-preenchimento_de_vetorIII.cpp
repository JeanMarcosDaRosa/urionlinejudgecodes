//Preenchimento de Vetor III
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

using namespace std;

int main()
{
    double v[100];
    double valor;
    scanf("%lf", &valor);
    v[0]=valor;
    for(int i=1;i<100;i++){
        v[i]=v[i-1]/2;
    }
    for(int i=0;i<100;i++){
        printf("N[%d] = %.4lf\n", i, v[i]);
    }
    return 0;
}
