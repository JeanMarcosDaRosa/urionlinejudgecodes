//Feynman
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    int valor, total;
    scanf("%d", &valor);
    while(valor>0){
        total = 0;
        for(int i=1; i<=valor;i++){
            total+=pow(i,2);
        }
        printf("%d\n", total);
        scanf("%d", &valor);
    }
    return 0;
}