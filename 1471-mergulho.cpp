//Mergulho

#include <stdio.h>
#include <stdlib.h>

int compare(const void *a, const void *b){
    return (*(int*)a-*(int*)b);
}

int main(){
    int n,r;
    while(scanf("%d %d", &n, &r)!=EOF){
        int v[r];
        for(int i=1;i<=r;i++){
            scanf("%d", &v[i-1]);
        }
        bool trocou = false;
        qsort(v, r, sizeof(int), compare);
        int c=0;
        for(int i=1;i<=n;i++){
            if(i==v[c]){
                c++;
            }else{
                printf("%d ", i);
                trocou=true;
            }
        }
        if(!trocou) printf("*\n");
        else printf("\n");
    }
    return 0;
}
